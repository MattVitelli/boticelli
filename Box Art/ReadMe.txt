Boxify Software
Copyright 2012
Created by Matt Vitelli

You can reach me at matthew.vitelli@gmail.com or mvitelli@stanford.edu

Please note that this software is provided as-is. It is not meant to be the end-all be-all, handle all edge cases, or be a general-purpose solution to vectorized art. It is just a working implementation that is suited to solving the problem of robotic art for the Boticelli project. The code is certainly not the most readable and contains many unused classes and data-structures. The code is primarily contained in Game1.cs. All the actual image processing occurs on both the GPU and CPU and is all contained within the PerformImageProcessing method. Nearly everything else is somehow tied to the WYSIWYG editor or rendering components or is unused.

The edge image is saved in the bin directory and must be passed along to vector magic for proper vectorization. The resulting svg can then be loaded and all the data will be saved via the File->Save Points command.

Please note that before running the program, you must first install either XNA Game Studio 3.1 Redistributable or XNA Game Studio 3.1 Developer Build. If you plan on modifying the code/loading the C# project, use the developer build. If you plan on simply running the application, use the redistributable build.

Here are the links to both:
XNA GS 3.1 Redistributable: http://www.microsoft.com/en-us/download/details.aspx?id=15163

XNA GS 3.1: http://www.microsoft.com/en-us/download/details.aspx?id=39


If you're only interested in running the software, you can find the application at /Boxify - Release Build/ThreePhase.exe