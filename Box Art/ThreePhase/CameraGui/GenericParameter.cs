﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ThreePhase.CameraGui
{
    public partial class GenericParameter : UserControl
    {
        public event EventHandler ValueChanged;

        public GenericParameter()
        {
            InitializeComponent();

            trackBar.ValueChanged += new EventHandler(OnValueChanged);
        }

        public GenericParameter(int minimum, int maximum)
        {
            InitializeComponent();
            SetRange(minimum, maximum);
            trackBar.ValueChanged += new EventHandler(OnValueChanged);
        }

        public string ParameterName
        {
            get { return label.Text; }
            set { label.Text = value; }
        }

        public int Value
        {
            get { return trackBar.Value; }
            set { trackBar.Value = value; }
        }

        public int Minimum
        {
            get { return trackBar.Minimum; }
            set { trackBar.Minimum = value; }
        }

        public int Maximum
        {
            get { return trackBar.Maximum; }
            set { trackBar.Maximum = value; }
        }

        public void SetRange(int minimum, int maximum)
        {
            trackBar.Minimum = minimum;
            trackBar.Maximum = maximum;
            trackBar.Value = (trackBar.Maximum + trackBar.Minimum) / 2;
        }

        private void OnValueChanged(object sender, EventArgs e)
        {
            if (!DesignMode && ValueChanged != null)
            this.Invoke(ValueChanged);
        }

        private void GenericParameter_SizeChanged(object sender, EventArgs e)
        {
            trackBar.Width = this.Width;
        }
    }
}
