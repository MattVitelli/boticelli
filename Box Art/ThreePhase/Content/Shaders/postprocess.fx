float2 InvRes;
float Radius;
float Threshold;
float2 Res;
float ColorRange;

float RedundantWeight;

#define NUM_POINTS 128
float4 Points[NUM_POINTS];

#define SQRT2 1.4142135623730950488
#define PI 3.14159265358979323846

texture HoughMap;
sampler HoughSampler = sampler_state
{
    Texture = (HoughMap);
    
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
    
};

texture TrigMap;
sampler TrigSampler = sampler_state
{
    Texture = (TrigMap);
    
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
    
};

texture baseMap : register(S0);
sampler BaseSampPoint = sampler_state
{
    Texture = (baseMap);
    
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
    
};

sampler BaseSampLin = sampler_state
{
    Texture = (baseMap);
    
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = LINEAR;
    MinFilter = LINEAR;
    Mipfilter = LINEAR;
    
};

struct VSIN
{
    float4 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
};

struct VSOUT
{
    float4 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
};

VSOUT VS(VSIN input)
{
    VSOUT output;
    output.Position = input.Position;
    output.TexCoord = input.TexCoord;
    return output;
}

struct VSLINEOUT
{
    float4 Position : POSITION0;
    float4 Color : TEXCOORD0;
};

VSLINEOUT LineVS(float4 inPosition : POSITION0, float4 inColor : COLOR)
{
    VSLINEOUT output;
    output.Position = inPosition;
    output.Color = inColor;
    return output;
}
const float gaussianBlur5x5[5][5] =
{
   0.000788907,   0.006581144,   0.013347322,   0.006581144,   0.000788907,
   0.006581144,   0.054900876,   0.111345294,   0.054900876,   0.006581144,
   0.013347322,   0.111345294,   0.225821052,   0.111345294,   0.013347322,
   0.006581144,   0.054900876,   0.111345294,   0.054900876,   0.006581144,
   0.000788907,   0.006581144,   0.013347322,   0.006581144,   0.000788907,
};

float4 ConvolvePS(VSOUT input) : COLOR
{
	float2 TC = input.TexCoord+0.5*InvRes;
	float4 color = 0;
	float2 cR = Radius*InvRes;
	for( int r = -2; r <= 2; r++ )
	for( int c = -2; c <= 2; c++ )
		color += tex2D(BaseSampLin, TC+cR*float2( r, c )) * gaussianBlur5x5[ r+2 ][ c+2 ];

    return color;
}

const float2 NNOff[8] =
{
	1,0,
	-1,0,
	0,1,
	0,-1,
	1,1,
	-1,-1,
	1,-1,
	-1,1,
};

float4 EdgeDetectPS(VSOUT input) : COLOR0
{
    float2 TC = input.TexCoord+0.5*InvRes;
    float3 avgValue = 0;
    
    float weight = 0;
    float variance = 0;

    avgValue = tex2D(BaseSampPoint, TC);
    for(int i = 0; i < 8; i++)
    {
		float v = length(tex2D(BaseSampPoint, TC+NNOff[i]*InvRes)- avgValue);
		v *= v;
		variance += v;
		weight++;
    }
    variance /= weight;

    
    return variance;
}

float4 EdgeThresholdPS(VSOUT input) : COLOR0
{
	float2 TC = input.TexCoord + 0.5*InvRes;
	return (tex2D(BaseSampPoint, TC).r > Threshold) ? float4(0,0,0,1) : float4(1,1,1,0);
}

float4 EdgeGradientPS(VSOUT input) : COLOR0
{
	float2 TC = input.TexCoord + 0.5*InvRes;
	float2 grad;
	grad.x = tex2D(BaseSampPoint, TC+InvRes*float2(1,0)).r - tex2D(BaseSampPoint, TC-InvRes*float2(1,0)).r;
	grad.y = tex2D(BaseSampPoint, TC-InvRes*float2(0,1)).r - tex2D(BaseSampPoint, TC+InvRes*float2(0,1)).r;
	grad = normalize(-grad);
	return float4(grad,0,0);
}

/*
float4 HoughMapPS(VSOUT input) : COLOR0
{
	float4 XParams = input.TexCoordX * XScale;
	float4 YParams = input.TexCoordY * YScale;
	
	float cost = 0;
	for(int i = 0; i < NUM_POINTS; i++)
	{
		float
		float2 diff = 
		cost += 
	}
}
*/
float4 HoughMapPS(VSOUT input) : COLOR0
{
	float2 TC = input.TexCoord;
	float2 Scale = float2(PI, SQRT2);
    float2 Axis = float2(TC.x, TC.y*2-1)*Scale;
    float estY = 0;
    float2 TCSamp = TC+0.5/Res;
    float2 trig = tex2D(TrigSampler, TCSamp);
    float cosTheta = trig.x;
    float sinTheta = trig.y;

    float4 outColor = tex2D(HoughSampler, TCSamp);
    outColor.yz = float2(TC.x*Res.x, Axis.y);
    
    for(int i = 0; i < NUM_POINTS; i++)
    {
		estY = Points[i].x*cosTheta+Points[i].y*sinTheta;
		if(abs(estY-Axis.y)*Res.y <= 1.75)
			outColor.x++;
		estY = Points[i].z*cosTheta+Points[i].w*sinTheta;
		if(abs(estY-Axis.y)*Res.y <= 1.75)
			outColor.x++;
    }
    //outColor.x -= RedundantWeight;
    return outColor;
}

float4 HoughSuppressionPS(VSOUT input) : COLOR0
{
	float2 TC = input.TexCoord+0.5*InvRes;
	float4 outColor = tex2D(HoughSampler, TC);
	float currI = outColor.x; //The currPixel
	float maxValue = currI;
	for(int i = 0; i < 8; i++)
	{
		maxValue = max(maxValue, tex2D(HoughSampler, TC+NNOff[i]*InvRes).x);
	}
	if(abs(maxValue-currI) > 0.01)
		outColor.x = 0;
	
	return outColor;
}

float4 ReduceColorPS(VSOUT input) : COLOR0
{
    float2 TC = input.TexCoord+0.5*InvRes;
    float4 color = tex2D(BaseSampPoint, TC);
    color *= ColorRange;
    color = floor(color);
    color /= ColorRange;
    return color;
}

float4 DrawImagePS(VSOUT input) : COLOR0
{
    float2 TC = input.TexCoord+0.5*InvRes;
    return tex2D(BaseSampLin, TC);
}

float4 DownsamplePS(VSOUT input) : COLOR0
{
    float2 TC = input.TexCoord+0.5*InvRes;
    float4 color = tex2D(BaseSampPoint, TC);
    color += tex2D(BaseSampPoint, TC+float2(InvRes.x,0));
    color += tex2D(BaseSampPoint, TC+float2(0,InvRes.y));
    color += tex2D(BaseSampPoint, TC+InvRes);
    return color/4;
}
/*
float4 MedianFilterPS(VSOUT input) : COLOR0
{
	const int MAX_COLORS = 8;
	const int FILTER_RADIUS = 1;
	float2 TC = input.TexCoord+0.5*InvRes;
	float sortedColors = 
}
*/
float4 DrawBlackPS(VSLINEOUT input) : COLOR0
{
    return input.Color;
}

technique HoughMapping
{
    pass Pass1
    {

        VertexShader = compile vs_2_0 VS();
        PixelShader = compile ps_3_0 HoughMapPS();
    }
}

technique HoughSuppress
{
    pass Pass1
    {

        VertexShader = compile vs_2_0 VS();
        PixelShader = compile ps_2_0 HoughSuppressionPS();
    }
}

technique Convolve5x5
{
    pass Pass1
    {

        VertexShader = compile vs_2_0 VS();
        PixelShader = compile ps_3_0 ConvolvePS();
    }
}

technique DrawBlack
{
    pass Pass1
    {

        VertexShader = compile vs_2_0 LineVS();
        PixelShader = compile ps_2_0 DrawBlackPS();
    }
}

technique EdgeDetect
{
    pass Pass1
    {

        VertexShader = compile vs_2_0 VS();
        PixelShader = compile ps_3_0 EdgeDetectPS();
    }
}

technique EdgeThreshold
{
    pass Pass1
    {

        VertexShader = compile vs_2_0 VS();
        PixelShader = compile ps_3_0 EdgeThresholdPS();
    }
}

technique EdgeGradient
{
    pass Pass1
    {

        VertexShader = compile vs_2_0 VS();
        PixelShader = compile ps_3_0 EdgeGradientPS();
    }
}

technique ReduceColors
{
    pass Pass1
    {

        VertexShader = compile vs_2_0 VS();
        PixelShader = compile ps_2_0 ReduceColorPS();
    }
}

technique DrawImage
{
    pass Pass1
    {

        VertexShader = compile vs_2_0 VS();
        PixelShader = compile ps_2_0 DrawImagePS();
    }
}

technique DownsampleImage
{
	pass Pass1
	{
		VertexShader = compile vs_2_0 VS();
        PixelShader = compile ps_2_0 DownsamplePS();
	}
}