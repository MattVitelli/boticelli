using System;
//using System.Drawing;
//using System.Drawing.Imaging;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using TouchlessLib;

namespace ThreePhase
{

    public class Game1 : Microsoft.Xna.Framework.Game
    {

        public static Game1 Instance
        {
            get
            {
                return instance;
            }
        }
        static Game1 instance;
        GraphicsDeviceManager graphics;
        QuadRenderComponent quad;
        RenderLine line;
        Effect ppEffect;
        RenderTarget2D image;
       
        KeyboardState lastState;

        Texture2D BaseImage;
        RenderTarget2D RT;
        DepthStencilBuffer DSBuffer;
        Texture2D HoughImage;
        LineData[] detectedLines;
        bool updateImage=false;
        float edgeThresh = 0.6f;
        float blurRadius = 1;
        HoughClassifier hough;
        //float lineThreshold = 8.6f;
        //int lineCount = 10;
        double[] sinTable = null;
        double[] cosTable = null;

        public float LineThreshold
        {
            get { return hough.LineThreshold; }
            set { hough.LineThreshold = value; updateImage = true;}
        }
        public int LineCount
        {
            get { return hough.TotalLines; }
            set { hough.TotalLines = value; updateImage = true; }
        }
        public float EdgeThreshold
        {
            get { return edgeThresh; }
            set { edgeThresh = value; updateImage = true; }
        }
        public float BlurRadius
        {
            get { return blurRadius; }
            set { blurRadius = value; updateImage = true; }
        }
        public bool drawHough = false;
        public bool drawEdge = false;

        InterfacePanel iPanel;

        public Game1()
        {
            instance = this;
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferMultiSampling = false;
            graphics.ApplyChanges();
            
            Content.RootDirectory = "Content";
            quad = new QuadRenderComponent();
            line = new RenderLine();
            this.IsMouseVisible = true;
        }

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            
            int _w, _h;
            _w = _h = 512;
            RT = new RenderTarget2D(GraphicsDevice, _w, _h, 1, SurfaceFormat.Color);
            DSBuffer = new DepthStencilBuffer(GraphicsDevice, _w, _h, DepthFormat.Depth24Stencil8);
            ppEffect = Content.Load<Effect>("Shaders/postprocess");

            iPanel = new InterfacePanel();
            iPanel.Show();
        }

        public void LoadTexture(string filename)
        {
            BaseImage = Texture2D.FromFile(GraphicsDevice, filename);
            RT = new RenderTarget2D(GraphicsDevice, BaseImage.Width, BaseImage.Height, 1, SurfaceFormat.Color);
            DSBuffer = new DepthStencilBuffer(GraphicsDevice, BaseImage.Width, BaseImage.Height, DepthFormat.Depth24Stencil8);
            updateImage = true;
        }

        void ComputeHough(Texture2D inTex)
        {
            Color[] colorData = new Color[inTex.Width * inTex.Height];
            inTex.GetData<Color>(colorData);
            List<Vector2> pointList = new List<Vector2>();
            Vector2 InvRes = Vector2.One/new Vector2(inTex.Width, inTex.Height);
            for (int x = 0; x < inTex.Width; x++)
            {
                for (int y = 0; y < inTex.Height; y++)
                {
                    if (colorData[x + y * inTex.Width].R > 128) //If we have an edge, add it to the list.
                        pointList.Add(new Vector2(x, y)*InvRes);//*2.0f-Vector2.One);
                }
            }
            HoughData[] houghMap = new HoughData[colorData.Length];

            /*
            double maxSlope = 10;
            for (int i = 0; i < pointList.Count; i++)
            {
                for (int m = 0; m < inTex.Height; m++)
                {
                    double slope = ((double)m / (double)inTex.Height);// *maxSlope;
                    double b = (-slope * pointList[i].Y + pointList[i].X)*0.5+0.5;
                    int bIdx = (int)b * inTex.Width;
                    houghMap[m + bIdx*inTex.Height]++;
                }
            }
            */
            
            double pMax = 2*Math.Sqrt(2); //We did our 0.5 multiplication here...
            
            for (int i = 0; i < pointList.Count; i++)
            {
                for(int theta = 0; theta < inTex.Width; theta++)
                {
                    //double thetRad = ((double)theta/(double)inTex.Width)*MathHelper.Pi;
                    double p = ((pointList[i].X * cosTable[theta] + pointList[i].Y * sinTable[theta])/pMax) + 0.5;
                    int pIdx = theta + (int)(p * inTex.Height) * inTex.Width;
                    houghMap[pIdx].Vote++;
                    houghMap[pIdx].Theta = theta;
                    houghMap[pIdx].Offset = p;
                }
            }

            int maxValue = 0;
            HoughData[] houghCopy = new HoughData[houghMap.Length];
            for (int i = 0; i < houghMap.Length; i++)
            {
                houghCopy[i] = houghMap[i];
                //if (houghMap[i].Vote > maxValue)
                //    maxValue = houghMap[i].Vote;
            }
            
            //float maxValue = 0;
            HoughData[] sortedHough = RadixSort.Sort(houghCopy);
            detectedLines = null;
            bool terminateSearch = false;
            int idx = sortedHough.Length - 1;
            maxValue = sortedHough[idx].Vote;
            List<Vector3> paramList = new List<Vector3>(); //X = Theta Y = Radius Z = HoughIndex
            //int terminationCondition = 20; //If we detect 10 unique lines, we're good.
            int activeIndex = idx;
            while (!terminateSearch && activeIndex >= 0)
            {
                Vector2 crd = new Vector2((float)sortedHough[activeIndex].Theta, (float)((sortedHough[activeIndex].Offset - 0.5) * pMax * inTex.Height));
                float leastDistSqr = float.MaxValue;
                for (int i = 0; i < paramList.Count; i++)
                {
                    //Vector2 dist = crd-new Vector2(paramList[i].X, paramList[i].Y);
                    float distSqr = Vector2.Distance(crd, new Vector2(paramList[i].X, paramList[i].Y));//Vector2.Dot(dist, dist);
                    leastDistSqr = Math.Min(leastDistSqr, distSqr);
                }
                if (leastDistSqr > LineThreshold) //We've got a unique line! Let's add it.
                {
                    paramList.Add(new Vector3(crd, activeIndex));
                    if(paramList.Count >= LineCount)
                        terminateSearch = true;
                }
                activeIndex--;

            }

            List<LineData> lines = new List<LineData>();
            
            for (int i = 0; i < paramList.Count; i++)
            {
                //Console.WriteLine("Value " + i + ":     " + (float)sortedHough[idx - i].Vote / (float)maxValue);
                int theta = (int)paramList[i].X;//sortedHough[idx-i].Theta;
                double thetRad = (double)theta * MathHelper.Pi / (double)inTex.Width;
                double rad = (sortedHough[(int)paramList[i].Z].Offset-0.5)*pMax;
                Vector2 start, end;
                if (thetRad > MathHelper.PiOver4 && thetRad < MathHelper.PiOver4 * 3f) //We're in a nasty range..switch to cot approximations and X functions for better accuracy
                {
                    float m = (float)(-sinTable[theta]/cosTable[theta]);
                    float b = (float)(rad / cosTable[theta]);
                    start.Y = 0;
                    start.X = b;
                    end.Y = 1;
                    end.X = m * end.Y + b;
                }
                else
                {
                    float m = (float)(-cosTable[theta]/sinTable[theta]);
                    float b = (float)(rad / sinTable[theta]);
                    start.X = 0;
                    start.Y = b;
                    end.X = 1;
                    end.Y = m * end.X + b;
                }
                LineData line = new LineData();
                line.start = start*2-Vector2.One;
                line.start.Y *= -1;
                line.end = end*2-Vector2.One;
                line.end.Y *= -1;
                /*
                line.start = new Vector2((float)(rad*cosTable[theta]), (float)(rad*sinTable[theta]));
                line.end = -1f*line.start;
                */
                lines.Add(line);
            }
            detectedLines = lines.ToArray();
            //List<Vector2> parameterLists = new List<Vector2>(); //Contains theta in X and R in Y.
            //while (!terminateSearch)
            {

            }
            //maxValue = sortedHough[sortedHough.Length - 1]; //sorted in ascending order, so last index is greatest value.
            //maxValue /= 2.0f;
            float maxHalf = (float)maxValue / 2.0f;

            Color[] houghColor = new Color[houghMap.Length];
            for (int i = 0; i < houghMap.Length; i++)
            {
                float val = (float)houghMap[i].Vote / (float)maxValue;
                houghColor[i] = new Color(val, val, val, 1.0f);
            }
            HoughImage = new Texture2D(GraphicsDevice, inTex.Width, inTex.Height, 1, TextureUsage.None, SurfaceFormat.Color);
            HoughImage.SetData<Color>(houghColor);
            //RadixSort.Sort(colorData); //Get our lines into a nice ordered buffer
            
        }

        protected override void Update(GameTime gameTime)
        {
            float time = gameTime.ElapsedGameTime.Milliseconds;
            KeyboardState keystate = Keyboard.GetState();
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();
            MouseState m = Mouse.GetState();

            lastState = keystate;
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.RenderState.CullMode = CullMode.None;
               
            DepthStencilBuffer old = GraphicsDevice.DepthStencilBuffer;
            if (updateImage)
            {
                GraphicsDevice.DepthStencilBuffer = DSBuffer;

                GraphicsDevice.SetRenderTarget(0, RT);
                ppEffect.Parameters["baseMap"].SetValue(BaseImage);
                ppEffect.Parameters["Radius"].SetValue(blurRadius);
                ppEffect.Parameters["InvRes"].SetValue(Vector2.One / new Vector2(RT.Width, RT.Height));
                ppEffect.CurrentTechnique = ppEffect.Techniques["Convolve5x5"];
                ppEffect.Begin();
                ppEffect.CurrentTechnique.Passes[0].Begin();
                quad.Render(Vector2.One * -1, Vector2.One);
                ppEffect.CurrentTechnique.Passes[0].End();
                ppEffect.End();
                GraphicsDevice.SetRenderTarget(0, null);
                
                ppEffect.Parameters["baseMap"].SetValue(RT.GetTexture());
                ppEffect.Parameters["Threshold"].SetValue(edgeThresh);
                ppEffect.Parameters["InvRes"].SetValue(Vector2.One / new Vector2(RT.Width, RT.Height));
                ppEffect.CurrentTechnique = ppEffect.Techniques["EdgeDetect"];
                GraphicsDevice.SetRenderTarget(0, RT);
                ppEffect.Begin();
                ppEffect.CurrentTechnique.Passes[0].Begin();
                quad.Render(Vector2.One * -1, Vector2.One);
                ppEffect.CurrentTechnique.Passes[0].End();
                ppEffect.End();
                GraphicsDevice.SetRenderTarget(0, null);
                hough.ComputeHoughImage(RT.GetTexture());
                //ComputeHough(RT.GetTexture());

                updateImage = false;
                GraphicsDevice.DepthStencilBuffer = old;
            }
            if (BaseImage != null)
            {
                ppEffect.Parameters["baseMap"].SetValue(BaseImage);
                ppEffect.Parameters["InvRes"].SetValue(Vector2.One / new Vector2(BaseImage.Width, BaseImage.Height));
                ppEffect.CurrentTechnique = ppEffect.Techniques["DrawImage"];
                ppEffect.Begin();
                ppEffect.CurrentTechnique.Passes[0].Begin();
                quad.Render(Vector2.One * -1, Vector2.One);
                ppEffect.CurrentTechnique.Passes[0].End();
                ppEffect.End();
            }
            if (drawEdge && RT != null)
            {
                ppEffect.Parameters["baseMap"].SetValue(RT.GetTexture());
                ppEffect.Parameters["InvRes"].SetValue(Vector2.One / new Vector2(RT.Width, RT.Height));
                ppEffect.CurrentTechnique = ppEffect.Techniques["DrawImage"];
                ppEffect.Begin();
                ppEffect.CurrentTechnique.Passes[0].Begin();
                quad.Render(Vector2.One * -1, Vector2.One);
                ppEffect.CurrentTechnique.Passes[0].End();
                ppEffect.End();
            }
            if (drawHough && hough.HoughImage != null)
            {
                ppEffect.Parameters["baseMap"].SetValue(hough.HoughImage);
                ppEffect.Parameters["InvRes"].SetValue(Vector2.One / new Vector2(hough.HoughImage.Width, hough.HoughImage.Height));
                ppEffect.CurrentTechnique = ppEffect.Techniques["DrawImage"];
                ppEffect.Begin();
                ppEffect.CurrentTechnique.Passes[0].Begin();
                quad.Render(Vector2.One * -1, Vector2.One);
                ppEffect.CurrentTechnique.Passes[0].End();
                ppEffect.End();
            }
            if (hough.Lines != null)
            {
                ppEffect.CurrentTechnique = ppEffect.Techniques["DrawBlack"];
                ppEffect.Begin();
                ppEffect.CurrentTechnique.Passes[0].Begin();
                for (int i = 0; i < hough.Lines.Length; i++)
                    line.Render(hough.Lines[i].start, hough.Lines[i].end);
                ppEffect.CurrentTechnique.Passes[0].End();
                ppEffect.End();
            }
            base.Draw(gameTime);
        }
    }
}
