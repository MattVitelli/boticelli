using System;
//using System.Drawing;
//using System.Drawing.Imaging;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Linq;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using ThreePhase.Utils;

namespace ThreePhase
{
    public enum ShapeMode
    {
        Box,
        Circle,
        Spiral,
        FunkySpiral,
        ZuluParlorTrick,
    };

    public enum LineStyle
    {
        Normal,
        Waves,
        Jaggies,
    };

    public struct ColorMode
    {
        public string name;
        public Color color;
        public Vector3 colorHSL;
        public Vector3 colorXYZ;

        public ColorMode(string name, Color color)
        {
            this.name = name;
            this.color = color;
            this.colorHSL = ColorUtil.RGBToHSL(color.ToVector3());
            this.colorXYZ = ColorUtil.RGBToXYZ(color.ToVector3());
        }
    };

    public class Game1 : Microsoft.Xna.Framework.Game
    {

        public static Game1 Instance
        {
            get
            {
                return instance;
            }
        }
        static Game1 instance;
        GraphicsDeviceManager graphics;
        QuadRenderComponent quad;
        RenderLine line;
        Effect ppEffect;
       
        KeyboardState lastState;

        Texture2D BaseImage;
        RenderTarget2D RT;
        RenderTarget2D RTEdge;
        RenderTarget2D RTPing;
        DepthStencilBuffer DSBuffer;
        
        bool updateShapes = false;
        bool updateEdges = false;
        float edgeThresh = 0.6f;
        float blurRadius = 1;

        ColorMode[] availableColors;

        SortedList<int, ShapeMode> artisticStyle;

        LineStyle lineStyle = LineStyle.Normal;

        public List<LineData[]> Outlines = null;        

        public bool DrawOutlines = false;

        int searchRadius = 5;

        public int SearchRadius
        {
            get { return searchRadius; }
            set { searchRadius = value; updateShapes = true; }
        }

        public LineStyle ArtisticFlare
        {
            get { return lineStyle; }
            set { lineStyle = value; updateShapes = true; }
        }

        public float EdgeThreshold
        {
            get { return edgeThresh; }
            set { edgeThresh = value; updateEdges = true; }
        }
        public float BlurRadius
        {
            get { return blurRadius; }
            set { blurRadius = value; updateEdges = true; updateShapes = true; }
        }

        int numShapes = 4;

        public int ShapeCount
        {
            get { return numShapes; }
            set { numShapes = value; updateShapes = true; }
        }

        int numPointsPerShape = 12;

        public int ShapeResolution
        {
            get { return numPointsPerShape; }
            set { numPointsPerShape = value; updateShapes = true; }
        }

        ShapeMode shapeMode = ShapeMode.Spiral;

        public ShapeMode Shape
        {
            get { return shapeMode; }
            set { shapeMode = value; updateShapes = true; }
        }

        public bool DrawWYSIWYG
        {
            get { return drawHough; }
            set { drawHough = value; settingsChanged = true; }
        }
        
        public bool DrawEdges
        {
            get { return drawEdge; }
            set { drawEdge = value; settingsChanged = true; }
        }

        bool settingsChanged = false;
        bool drawHough = false;
        bool drawEdge = false;
        
        InterfacePanel iPanel;

        LineData[] detectedLines;

        public bool DrawShapes = false;
        
        public ColorMode[] GetColorPalette()
        {
            return availableColors;
        }

        public LineData[] GetDetectedLines()
        {
            return detectedLines;
        }

        public Game1()
        {
            instance = this;
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferMultiSampling = false;
            graphics.ApplyChanges();
            
            Content.RootDirectory = "Content";
            quad = new QuadRenderComponent();
            line = new RenderLine();
            this.IsMouseVisible = true;
        }

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            
            int _w, _h;
            _w = _h = 512;
            RT = new RenderTarget2D(GraphicsDevice, _w, _h, 1, SurfaceFormat.Color);
            DSBuffer = new DepthStencilBuffer(GraphicsDevice, _w, _h, DepthFormat.Depth24Stencil8);
            ppEffect = Content.Load<Effect>("Shaders/postprocess");

            LoadColorPalette("palette.txt");
            LoadArtisticStyle("style.txt");
            iPanel = new InterfacePanel();
            iPanel.Show();
            
            //DebugBezier();
        }

        Vector2 EvaluateBezier(Vector2[] points, float t)
        {
            Vector2 pt = Vector2.Zero;
            float invT = 1.0f - t;
            pt += (float)Math.Pow(invT, 5) * points[0];
            pt += (float)(5 * t * Math.Pow(invT, 4)) * points[1];
            pt += (float)(10 * Math.Pow(t,2) * Math.Pow(invT, 3)) * points[2];
            pt += (float)(10 * Math.Pow(t,3) * Math.Pow(invT, 2)) * points[3];
            pt += (float)(5 * Math.Pow(t,4) * invT) * points[4];
            pt += (float)Math.Pow(t,5) * points[5];
            return pt;
        }
        /*
        void DebugBezier()
        {
            List<LineData[]> lines = SVGParser.ParseSVG("edges.svg");
            List<LineData> linesNew = new List<LineData>();
            for (int i = 0; i < lines.Count; i++)
            {
                linesNew.AddRange(lines[i]);
            }
            detectedLines = linesNew.ToArray();
            Vector2[] testPoints = { Vector2.Zero, Vector2.One * 0.25f, new Vector2(1, 0.5f), new Vector2(3, 3), new Vector2(0, 3), new Vector2(4, 4) };
            float[] tVals = new float[] { 0.0f, 0.5f, 1.0f };
            BezierCurveUtil.Test(testPoints, tVals);
            Console.WriteLine("EVALUATING CONTROL POINTS");
            for (int i = 0; i < tVals.Length; i++)
            {
                Vector2 pt = EvaluateBezier(testPoints, tVals[i]);
                Console.WriteLine("f({0})->{1}", tVals[i], pt);
            }
        }
        */
        string ToHex(uint number)
        {
            string text = "0x";
            uint clearFlag = 0xF;

            int dataSize = sizeof(uint)*2;
            for (int i = dataSize - 1; i >= 0; i--)
            {
                uint flag = (number >> (i * 4)) & clearFlag;
                text += (flag >= 10) ? 'A' + flag - 10 : '0' + flag;
            }
           
            return text;
        }

        void GenerateLines()
        {
            Texture2D lines = new Texture2D(GraphicsDevice, 3, 3);
            Color[] color = new Color[lines.Width * lines.Height];
            uint count = 0x000001ff;
            for (uint i = 0; i < count; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    color[j] = (((i >> j) & 1) == 1) ? Color.White : Color.Black;
                }
                lines.SetData<Color>(color);
                byte[] bytes = BitConverter.GetBytes(i);
                lines.Save("Lines/line_" + BitConverter.ToString(new byte[] {bytes[1], bytes[0]}) + ".png", ImageFileFormat.Png);
            }
        }

        ShapeMode ParseShapeModeFromString(string name)
        {
            switch (name.ToLower())
            {
                case "box":
                    return ShapeMode.Box;
                case "line":
                    return ShapeMode.ZuluParlorTrick;
                case "circle":
                    return ShapeMode.Circle;
                case "spiral":
                    return ShapeMode.Spiral;
                case "funkyspiral":
                    return ShapeMode.FunkySpiral;
            }
            return ShapeMode.Box;
        }

        public void LoadArtisticStyle(string filename)
        {
            artisticStyle = new SortedList<int, ShapeMode>();
            SortedList<string, int> nameToID = new SortedList<string,int>();
            for (int i = 0; i < availableColors.Length; i++)
            {
                nameToID.Add(availableColors[i].name, GetColorID(availableColors[i].color));
            }

            using (FileStream fs = new FileStream(filename, FileMode.Open))
            {
                using (StreamReader reader = new StreamReader(fs))
                {
                    while (!reader.EndOfStream)
                    {
                        string[] data = reader.ReadLine().Split(' ');
                        if (nameToID.ContainsKey(data[0]))
                        {
                            int id = nameToID[data[0]];
                            if (!artisticStyle.ContainsKey(id))
                                artisticStyle.Add(id, ParseShapeModeFromString(data[1]));
                        }
                    }
                }
            }
        }

        public void LoadColorPalette(string filename)
        {
            availableColors = null;
            List<ColorMode> colorPalette = new List<ColorMode>();
            using (FileStream fs = new FileStream(filename, FileMode.Open))
            {
                using (StreamReader reader = new StreamReader(fs))
                {
                    while (!reader.EndOfStream)
                    {
                        //char[] splitTolkens = new char[] { ' ', '\t', };
                        string[] tolkens = reader.ReadLine().Split(' ');
                        Color color = new Color(byte.Parse(tolkens[1]), byte.Parse(tolkens[2]), byte.Parse(tolkens[3]));
                        colorPalette.Add(new ColorMode(tolkens[0], color));
                    }
                    availableColors = colorPalette.ToArray();
                }
            }
            
        }

        public void LoadTexture(string filename)
        {
            BaseImage = Texture2D.FromFile(GraphicsDevice, filename);
            int width = BaseImage.Width;// / 2;
            int height = BaseImage.Height;// / 2;
            RT = new RenderTarget2D(GraphicsDevice, width, height, 1, SurfaceFormat.Color);
            RTPing = new RenderTarget2D(GraphicsDevice, RT.Width, RT.Height, 1, RT.Format);
            RTEdge = new RenderTarget2D(GraphicsDevice, RT.Width, RT.Height, 1, SurfaceFormat.Single);
            DSBuffer = new DepthStencilBuffer(GraphicsDevice, RT.Width, RT.Height, DepthFormat.Depth24Stencil8);
            updateEdges = true;
            updateShapes = true;
        }

        protected override void Update(GameTime gameTime)
        {
            float time = gameTime.ElapsedGameTime.Milliseconds;
            KeyboardState keystate = Keyboard.GetState();
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();
            MouseState m = Mouse.GetState();

            lastState = keystate;
            base.Update(gameTime);
        }

        void DrawBlurredImage(Texture2D image, float radius)
        {
            ppEffect.Parameters["baseMap"].SetValue(image);
            ppEffect.Parameters["Radius"].SetValue(radius);
            ppEffect.Parameters["InvRes"].SetValue(Vector2.One / new Vector2(image.Width, image.Height));
            ppEffect.CurrentTechnique = ppEffect.Techniques["Convolve5x5"];
            
            ppEffect.Begin();
            ppEffect.CurrentTechnique.Passes[0].Begin();
            quad.Render(Vector2.One * -1, Vector2.One);
            ppEffect.CurrentTechnique.Passes[0].End();
            ppEffect.End();
        }

        void ReduceColorRange(RenderTarget2D renderTarget, float range)
        {
            ppEffect.Parameters["baseMap"].SetValue(renderTarget.GetTexture());
            ppEffect.Parameters["ColorRange"].SetValue(range);
            ppEffect.Parameters["InvRes"].SetValue(Vector2.One / new Vector2(renderTarget.Width, renderTarget.Height));
            ppEffect.CurrentTechnique = ppEffect.Techniques["ReduceColors"];
            GraphicsDevice.SetRenderTarget(0, renderTarget);
            ppEffect.Begin();
            ppEffect.CurrentTechnique.Passes[0].Begin();
            quad.Render(Vector2.One * -1, Vector2.One);
            ppEffect.CurrentTechnique.Passes[0].End();
            ppEffect.End();
            GraphicsDevice.SetRenderTarget(0, null);
        }

        void DrawEdgeImage(Texture2D image, float edgeThreshold)
        {
            GraphicsDevice.SetRenderTarget(0, RTEdge);
            ppEffect.Parameters["baseMap"].SetValue(image);
            ppEffect.Parameters["Threshold"].SetValue(edgeThreshold);
            ppEffect.Parameters["InvRes"].SetValue(Vector2.One / new Vector2(image.Width, image.Height));
            ppEffect.CurrentTechnique = ppEffect.Techniques["EdgeDetect"];

            ppEffect.Begin();
            ppEffect.CurrentTechnique.Passes[0].Begin();
            quad.Render(Vector2.One * -1, Vector2.One);
            ppEffect.CurrentTechnique.Passes[0].End();
            ppEffect.End();
            GraphicsDevice.SetRenderTarget(0, null);

            GraphicsDevice.SetRenderTarget(0, RTPing);
            ppEffect.Parameters["baseMap"].SetValue(RTEdge.GetTexture());
            ppEffect.Parameters["Threshold"].SetValue(edgeThreshold);
            ppEffect.CurrentTechnique = ppEffect.Techniques["EdgeThreshold"];

            ppEffect.Begin();
            ppEffect.CurrentTechnique.Passes[0].Begin();
            quad.Render(Vector2.One * -1, Vector2.One);
            ppEffect.CurrentTechnique.Passes[0].End();
            ppEffect.End();
            GraphicsDevice.SetRenderTarget(0, null);

            Color[] edgeData = new Color[RTPing.Width * RTPing.Height];
            RTPing.GetTexture().GetData<Color>(edgeData);
            for (int i = 0; i < edgeData.Length; i++)
            {
                edgeData[i].R = (byte)(255 - edgeData[i].R);
                edgeData[i].G = (byte)(255 - edgeData[i].G);
                edgeData[i].B = (byte)(255 - edgeData[i].B);
                edgeData[i].A = 255;
            }
            Texture2D tex = new Texture2D(GraphicsDevice, RTPing.Width, RTPing.Height, 1, TextureUsage.None, SurfaceFormat.Color);
            tex.SetData<Color>(edgeData);
            tex.Save("Edges.png", ImageFileFormat.Png);
        }

        LineData[] GetSpiral(Vector2 centerPos, float radius, Color color, bool isFunky)
        {
            int pointsPerSpiral = numPointsPerShape;
            int numSpirals = numShapes;

            float spiralDT = MathHelper.TwoPi / (float)pointsPerSpiral;
            float radiusDT = radius / (numSpirals*pointsPerSpiral); 
            
            List<LineData> lines = new List<LineData>();
            for (int i = 0; i < numSpirals; i++)
            {
                for (int j = 0; j < pointsPerSpiral; j++)
                {
                    Vector2 start = centerPos;
                    if (j > 0)
                        start = lines[lines.Count - 1].end;
                    float t = (float)j * spiralDT;
                    float rad = radiusDT * (float)(i*pointsPerSpiral + j);
                    if(isFunky)
                        rad = radiusDT * (float)(i * j);
                    Vector2 end = new Vector2((float)Math.Cos(t), (float)Math.Sin(t)) * rad + centerPos;
                    LineData line = new LineData();
                    line.start = start;
                    line.end = end;
                    line.color = color;
                    lines.Add(line);
                }
            }
            return lines.ToArray();
        }

        LineData[] CreateBox(Vector2 min, Vector2 max, Color color)
        {
            LineData[] box = new LineData[4];
            box[0].start = min;
            box[0].end = new Vector2(min.X, max.Y);
            box[0].color = color;
            box[1].start = min;
            box[1].end = new Vector2(max.X, min.Y);
            box[1].color = color;
            box[2].start = new Vector2(max.X, min.Y);
            box[2].end = max;
            box[2].color = color;
            box[3].start = new Vector2(min.X, max.Y);
            box[3].end = max;
            box[3].color = color;
            return box;
        }

        LineData[] GetBox(Vector2 centerPos, Vector2 min, Vector2 max, Color color)
        {
            int numBoxes = numShapes;
            Vector2 deltaMin = min / (float)numBoxes;
            Vector2 deltaMax = max / (float)numBoxes;
            LineData[] lines = new LineData[numBoxes * 4];
            for (int i = 1; i <= numBoxes; i++)
            {
                Vector2 currMin = deltaMin * i + centerPos;
                Vector2 currMax = deltaMax * i + centerPos;
                LineData[] box = CreateBox(currMin, currMax, color);
                Array.Copy(box, 0, lines, (i - 1) * 4, 4);
            }

            return lines;
        }

        LineData[] GetCircle(Vector2 centerPos, float radius, Color color)
        {
            int pointsPerSpiral = numPointsPerShape;
            int numSpirals = numShapes;

            float spiralDT = MathHelper.TwoPi / (float)pointsPerSpiral;
            float radiusDT = radius / (numSpirals);

            List<LineData> lines = new List<LineData>();
            for (int i = 0; i < numSpirals; i++)
            {
                for (int j = 0; j <= pointsPerSpiral; j++)
                {
                    Vector2 start = centerPos;
                    if (j > 0)
                        start = lines[lines.Count - 1].end;
                    float t = (float)j * spiralDT;
                    //float rad = radiusDT * (float)(i*pointsPerSpiral + j);
                    float rad = radiusDT * (float)(i+1);
                    Vector2 end = new Vector2((float)Math.Cos(t), (float)Math.Sin(t)) * rad + centerPos;
                    LineData line = new LineData();
                    line.start = start;
                    line.end = end;
                    line.color = color;
                    lines.Add(line);
                }
            }
            return lines.ToArray();
        }

        /*
        struct ArtistShape
        {
            Vector2 origin;
            LineData[] lines;
        }
        */

        LineData[] DrawShadeLinesFromImage(Texture2D image)
        {
            Color[] colorData = new Color[image.Width * image.Height];
            image.GetData<Color>(colorData);

            int boxRadius = searchRadius;

            Vector2 invRes = Vector2.One / new Vector2(image.Width, image.Height);

            List<LineData> linesToDraw = new List<LineData>();
            SortedList<int, char> visitedList = new SortedList<int, char>();
            //SortedList<int, List<Shape>> lineCollections = new SortedList<int, List<Shape>>();
            for (int x = 0; x < image.Width; x++)
            {
                for (int y = 0; y < image.Height; y++)
                {
                    int index = x + y * image.Width;
                    if (!visitedList.ContainsKey(index))
                    {
                        visitedList.Add(index, '1');
                        Color currColor = colorData[index];
                        float radius = 0;
                        Vector2 minPos = Vector2.Zero;
                        Vector2 maxPos = Vector2.Zero;
                        Vector2 pos = new Vector2(x, y)*invRes*2.0f - Vector2.One;
                        for (int i = -boxRadius; i <= boxRadius; i++)
                        {
                            int shiftX = x + i;
                            if (shiftX >= 0 && shiftX < image.Width)
                            {
                                for (int j = -boxRadius; j <= boxRadius; j++)
                                {
                                    int shiftY = y + j;
                                    if (shiftY >= 0 && shiftY < image.Height)
                                    {
                                        int offsetIndex = shiftX + shiftY * image.Width;
                                        Color sampColor = colorData[offsetIndex];
                                        if (sampColor == currColor)
                                        {
                                            if(!visitedList.ContainsKey(offsetIndex))
                                                visitedList.Add(offsetIndex, '1');
                                            Vector2 offset = new Vector2(shiftX, shiftY) * invRes * 2.0f - Vector2.One;
                                            float rad = Vector2.Distance(offset, pos);
                                            if (rad > radius)
                                                radius = rad;
                                            Vector2 sizeOffset = offset - pos;
                                            minPos = Vector2.Min(minPos, sizeOffset);
                                            maxPos = Vector2.Max(maxPos, sizeOffset);

                                        }
                                    }
                                }
                            }
                        }
                        pos = pos * new Vector2(1, -1);

                        int colorID = GetColorID(currColor);
                        ShapeMode currShape = shapeMode;// artisticStyle[colorID];
                        LineData[] currLines = null;
                        switch (currShape)
                        {
                            case ShapeMode.Box:
                                currLines = GetBox(pos, minPos, maxPos, currColor);
                                break;
                            case ShapeMode.Circle:
                                currLines = GetCircle(pos, radius, currColor);
                                break;
                            case ShapeMode.FunkySpiral:
                                currLines = GetSpiral(pos, radius, currColor, true);
                                break;
                            case ShapeMode.Spiral:
                                currLines = GetSpiral(pos, radius, currColor, false);
                                break;
                        }

                        if (currLines != null)
                        {
                            //float randTheta = (float)RandomUtil.RandomGen.NextDouble() * MathHelper.TwoPi;
                            //RotateLinesAroundCenter(ref currLines, pos, randTheta);
                            linesToDraw.AddRange(currLines);
                        }
                        //GetCircle(pos, radius, currColor));
                        //linesToDraw.AddRange(GetSpiral(pos, radius, currColor));
                        //linesToDraw.AddRange(GetBox(pos, minPos, maxPos, currColor));
                        //if(currColor.B  < 150)
                        //    linesToDraw.AddRange(GetCircle(pos, radius, currColor));//GetSpiral(pos, radius, currColor));
                    }
                }
            }
            return linesToDraw.ToArray();
        }

        public float GetImageAspectRatio()
        {
            if (BaseImage != null)
                return (float)BaseImage.Width / (float)BaseImage.Height;
            return 1;
        }

        int GetNearestPowerOfTwo(int value)
        {
            int bestFlag = 0;
            for (int i = 0; i < sizeof(int) * 8; i++)
            {
                int flag = 1 << i;
                if (flag > value)
                    break;
                if ((flag & value) > 0)
                    bestFlag = flag;
            }
            return bestFlag;
        }

        int GetShiftsToTarget(int srcValue, int destValue)
        {
            int numShifts = 0;
            while (srcValue > destValue)
            {
                srcValue = srcValue >> 1;
                numShifts++;
            }
            return numShifts;
        }

        Texture2D DownsampleImage(Texture2D image, int targetRes)
        {
            int minSize = Math.Min(image.Width, image.Height);
            int nearestPowerOfTwo = GetNearestPowerOfTwo(minSize);
            int numTargets = GetShiftsToTarget(nearestPowerOfTwo, targetRes) + 1;
            RenderTarget2D[] downsampledImages = new RenderTarget2D[numTargets];
            int res = nearestPowerOfTwo;
            for (int i = 0; i < downsampledImages.Length; i++)
            {
                downsampledImages[i] = new RenderTarget2D(GraphicsDevice, res, res, 1, SurfaceFormat.Color);
                res /= 2;
            }
            GraphicsDevice.SetRenderTarget(0, downsampledImages[0]);
            DrawImage(image);
            GraphicsDevice.SetRenderTarget(0, null);

            for (int i = 1; i < downsampledImages.Length; i++)
            {
                
                GraphicsDevice.SetRenderTarget(0, downsampledImages[i]);
                Texture2D currImage = downsampledImages[i - 1].GetTexture();
                ppEffect.Parameters["baseMap"].SetValue(currImage);
                ppEffect.Parameters["InvRes"].SetValue(Vector2.One / new Vector2(currImage.Width, currImage.Height));
                ppEffect.CurrentTechnique = ppEffect.Techniques["DownsampleImage"];
                ppEffect.Begin();
                ppEffect.CurrentTechnique.Passes[0].Begin();
                quad.Render(Vector2.One * -1, Vector2.One);
                ppEffect.CurrentTechnique.Passes[0].End();
                ppEffect.End();
                GraphicsDevice.SetRenderTarget(0, null);
            }

            RenderTarget2D smallestTarget = downsampledImages[downsampledImages.Length-1];

            return smallestTarget.GetTexture();

        }

        Texture2D PerformMedianFiltering(Texture2D image, int sampleRadius)
        {
            Color[] colorData = new Color[image.Width * image.Height];
            Color[] colorDataMedian = new Color[colorData.Length];
            image.GetData<Color>(colorData);
            for (int x = 0; x < image.Width; x++)
            {
                for (int y = 0; y < image.Height; y++)
                {
                    List<byte> sortedReds = new List<byte>();
                    List<byte> sortedGreens = new List<byte>();
                    List<byte> sortedBlues = new List<byte>();
                    for (int i = -sampleRadius; i <= sampleRadius; i++)
                    {
                        int shiftX = x + i;
                        if (shiftX >= 0 && shiftX < image.Width)
                        {
                            for (int j = -sampleRadius; j <= sampleRadius; j++)
                            {
                                int shiftY = y + j;
                                if (shiftY >= 0 && shiftY < image.Height)
                                {
                                    Color currColor = colorData[shiftX + shiftY * image.Width];
                                    sortedReds.Add(currColor.R);
                                    sortedGreens.Add(currColor.G);
                                    sortedBlues.Add(currColor.B);
                                }
                            }
                        }
                    }
                    sortedReds.Sort();
                    sortedGreens.Sort();
                    sortedBlues.Sort();
                    int medianIndex = sortedReds.Count / 2;
                    int index = x + y * image.Width;
                    colorDataMedian[index] = new Color(sortedReds[medianIndex], sortedGreens[medianIndex], sortedBlues[medianIndex]);
                }
            }

            Texture2D medianImage = new Texture2D(GraphicsDevice, image.Width, image.Height, 1, TextureUsage.None, SurfaceFormat.Color);
            medianImage.SetData<Color>(colorDataMedian);
            return medianImage;
        }

        Texture2D PerformModeFiltering(Texture2D image, int sampleRadius)
        {
            Color[] colorData = new Color[image.Width * image.Height];
            Color[] colorDataMode = new Color[colorData.Length];
            image.GetData<Color>(colorData);
            for (int x = 0; x < image.Width; x++)
            {
                for (int y = 0; y < image.Height; y++)
                {

                    SortedList<int, Color> colors = new SortedList<int,Color>();
                    SortedList<int, int> colorFrequencies = new SortedList<int, int>();
                    for (int i = -sampleRadius; i <= sampleRadius; i++)
                    {
                        int shiftX = x + i;
                        if (shiftX >= 0 && shiftX < image.Width)
                        {
                            for (int j = -sampleRadius; j <= sampleRadius; j++)
                            {
                                int shiftY = y + j;
                                if (shiftY >= 0 && shiftY < image.Height)
                                {
                                    Color currColor = colorData[shiftX + shiftY * image.Width];
                                    int colorIndex = currColor.R + 256 * (currColor.G + currColor.B * 256);
                                    if(!colorFrequencies.ContainsKey(colorIndex))
                                    {
                                        colors.Add(colorIndex, currColor);
                                        colorFrequencies.Add(colorIndex, 0);
                                    }
                                    colorFrequencies[colorIndex]++;
                                }
                            }
                        }
                    }
                    int bestKey = colors.Keys[0];
                    for (int i = 0; i < colors.Keys.Count; i++)
                    {
                        int key = colors.Keys[i];
                        if (colorFrequencies[key] > colorFrequencies[bestKey])
                            bestKey = key;
                    }

                    int index = x + y * image.Width;
                    colorDataMode[index] = colors[bestKey];
                }
            }

            Texture2D modeImage = new Texture2D(GraphicsDevice, image.Width, image.Height, 1, TextureUsage.None, SurfaceFormat.Color);
            modeImage.SetData<Color>(colorDataMode);
            return modeImage;
        }

        Color LeastSquaresMatch(Vector3 cmpColor)
        {
            Vector3 cmpColorHSL = ColorUtil.RGBToHSL(cmpColor);
            Vector3 cmpColorXYZ = ColorUtil.RGBToXYZ(cmpColor);
            Vector3 cmpColorCie = ColorUtil.xyz2cie(cmpColorXYZ);
            int bestIndex = 0;
            float bestCost = float.PositiveInfinity;
            //float dominantChannel = Math.Max(Math.Max(cmpColor.X, cmpColor.Y), cmpColor.Z);
            //int index = (dominantChannel == cmpColor.X) ? 0 : (dominantChannel == cmpColor.Y) ? 1 : 2;
            Vector3 normalizedRGB = cmpColor / Vector3.Dot(Vector3.One / 3.0f, cmpColor);
            float lum = Vector3.Dot(cmpColor, new Vector3(0.299f, 0.587f, 0.114f)) * 3.4f;
            /*
            normalizedRGB.X = (float)Math.Pow(normalizedRGB.X, lum);
            normalizedRGB.Y = (float)Math.Pow(normalizedRGB.Y, lum);
            normalizedRGB.Z = (float)Math.Pow(normalizedRGB.Z, lum);
            */
            //cmpColor *= normalizedRGB;
            for (int i = 0; i < availableColors.Length; i++)
            {
                //float cost = Math.Min(Vector3.Distance(availableColors[i].colorHSL, cmpColorHSL), Vector3.Distance(availableColors[i].colorHSL, cmpColorHSL + Vector3.Right));
                Vector3 color = availableColors[i].color.ToVector3();
                //float channel = (index == 0) ? color.X : (index == 1) ? color.Y : color.Z;
                //float cost = Math.Abs(dominantChannel - channel);
                Vector3 diffVec = color - cmpColor;
                float cost = (diffVec * new Vector3(0.5f, 1, 0.5f) * diffVec).LengthSquared();
                //float cost = Vector3.Distance(color, cmpColor);
                //Vector3 nrmRGB = color / Vector3.Dot(Vector3.One / 3.0f, color);
                //float cost = Math.Abs(availableColors[i].colorHSL.X - cmpColorHSL.X);

                //float cost = Vector3.Distance(normalizedRGB, nrmRGB);

                Vector3 currCmpColor = ColorUtil.xyz2cie(availableColors[i].colorXYZ);
                //float cost = Vector3.Distance(cmpColorXYZ, availableColors[i].colorXYZ);
                //float cost = Vector3.Distance(cmpColorCie, currCmpColor);
                if (cost < bestCost)
                {
                    bestCost = cost;
                    bestIndex = i;
                }
            }
            return availableColors[bestIndex].color;
        }

        void LeastSquaresColors(Texture2D image)
        {
            Color[] colorData = new Color[image.Width * image.Height];
            image.GetData<Color>(colorData);
            for (int x = 0; x < image.Width; x++)
            {
                for (int y = 0; y < image.Height; y++)
                {
                    int index = x + y * image.Width;
                    colorData[index] = LeastSquaresMatch(colorData[index].ToVector3());
                }
            }
            image.SetData<Color>(colorData);
        }

        Texture2D CreateStarryNightImage()
        {
            Texture2D downsampled = DownsampleImage(RT.GetTexture(), 256);
            Texture2D median = PerformMedianFiltering(downsampled, 1);
            Texture2D median2 = PerformMedianFiltering(median, 1);
            Texture2D mode = PerformModeFiltering(median2, 1);
            mode.Save("Preprocessed.png", ImageFileFormat.Png);
            LeastSquaresColors(mode);
            mode.Save("Downsampled.png", ImageFileFormat.Png);
            return mode;
        }

        Color[] GetUniqueColors(ref Color[] colorData)
        {
            SortedList<int, Color> histBuckets = new SortedList<int, Color>();
            for (int i = 0; i < colorData.Length; i++)
            {
                int colorID = colorData[i].R * (colorData[i].G + 256 * colorData[i].B) * 256;
                if (histBuckets.ContainsKey(colorID))
                    histBuckets.Add(colorID, colorData[i]);
            }
            return histBuckets.Values.ToArray();
        }

        void PerformZuluSwap(ref LineData[] lines, bool swapEnds)
        {
            Random rand = new Random();
            int offsetMax = 2;
            if (swapEnds)
            {
                for (int i = 0; i < lines.Length; i++)
                {
                    int shiftIndex = Math.Min(i + rand.Next(1, offsetMax), lines.Length - 1);
                    if (i < lines.Length - 1 && Vector2.Distance(lines[i].end, lines[i + 1].end) < 0.075f)
                        lines[i].end = lines[i + 1].end;
                }
            }
            else
            {
                for (int i = 0; i < lines.Length; i++)
                {
                    int shiftIndex = Math.Min(i - rand.Next(1, offsetMax), lines.Length - 1);
                    if (i < lines.Length - 1 && Vector2.Distance(lines[i].start, lines[i + 1].start) < 0.075f)
                        lines[i].start = lines[i + 1].start;
                }
            }
        }

        LineData[] SubdivideLine(LineData line)
        {
            int subDivMax = 15;
            float length = Vector2.Distance(line.start, line.end);
            //if(length > 0.1f)
            {
                LineData[] lines = new LineData[subDivMax];
                for (int i = 0; i < subDivMax; i++)
                {
                    lines[i].color = line.color;
                    lines[i].start = (i - 1 >= 0) ? lines[i - 1].end : line.start;
                    lines[i].end = Vector2.Lerp(line.start, line.end, ((float)i + 1) / (float)subDivMax);
                }
                return lines;
            }
            
            return new LineData[]{line};
        }


        int GetColorID(Color color)
        {
            int colorID = color.R + (color.G + 256 * color.B) * 256;
            return colorID;
        }

        LineData[] ZuluParlorTrick(Texture2D image)
        {
            Color[] colors = new Color[image.Width * image.Height];
            image.GetData<Color>(colors);

            Vector2 invRes = 2.0f * Vector2.One / new Vector2(image.Width, image.Height);

            SortedList<int, char> visitedList = new SortedList<int, char>();
            SortedList<int, List<LineData>> lineCollections = new SortedList<int, List<LineData>>();
            
            for (int x = 0; x < image.Width; x++)
            {
                for (int y = 0; y < image.Height; y+= numShapes)
                {
                    int index = x + y * image.Width;

                    if (!visitedList.ContainsKey(index))// && artisticStyle[GetColorID(colors[index])] == ShapeMode.ZuluParlorTrick)
                    {
                        int endIter = image.Width - x;
                        for (int i = 0; i < endIter; i++)
                        {
                            int indexOffset = (x + i + y * image.Width);
                            if (colors[indexOffset] == colors[index] && i < endIter-1)
                                visitedList.Add(indexOffset, '1');
                            else
                            {
                                LineData line = new LineData();
                                line.color = colors[index];
                                line.end = (new Vector2(x, y) * invRes - Vector2.One) * new Vector2(1, -1);
                                line.start = (new Vector2(x + i - 1, y) * invRes - Vector2.One) * new Vector2(1, -1);
                                if (line.start != line.end)
                                {
                                    int colorID = GetColorID(colors[index]);
                                    if (!lineCollections.ContainsKey(colorID))
                                        lineCollections.Add(colorID, new List<LineData>());
                                    lineCollections[colorID].Add(line);
                                }
                                break;
                            }
                        }
                    }
                }
            }

            List<LineData> lines = new List<LineData>();
            Random rand = new Random();
            for (int i = 0; i < lineCollections.Keys.Count; i++)
            {
                int key = lineCollections.Keys[i];
                for (int j = 0; j < lineCollections[key].Count; j++)
                {
                    /*
                    LineData[] currLines = lineCollections[key][j];
                    if(rand.Next() % 2 > 0)
                        PerformZuluSwap(ref currLines, true);
                    else
                        PerformZuluSwap(ref currLines, false);
                    */
                    lines.AddRange(SubdivideLine(lineCollections[key][j]));
                }
            }
            /*
            switch (lineStyle)
            {
                case LineStyle.Waves:
                    detectedLines = GenerateWaves(lines.ToArray());
                    break;
                case LineStyle.Jaggies:

                    break;
                default:
                    
                    break;
            }*/
            return lines.ToArray();
        }

        Vector2 RotatePoint(Vector2 point, Vector2 center, float cosTheta, float sinTheta)
        {
            point -= center;
            Vector2 newPoint = point * new Vector2(cosTheta, sinTheta) + new Vector2(point.Y, point.X) * new Vector2(-sinTheta, cosTheta);
            return newPoint + center;
        }

        void RotateLinesAroundCenter(ref LineData[] lines, Vector2 center, float theta)
        {
            float cosTheta = (float)Math.Cos(theta);
            float sinTheta = (float)Math.Sin(theta);
            for (int i = 0; i < lines.Length; i++)
            {
                LineData line = lines[i];
                line.start = RotatePoint(line.start, center, cosTheta, sinTheta);
                line.end = RotatePoint(line.end, center, cosTheta, sinTheta);
                lines[i] = line;
            }
        }

        void PerformImageProcessing()
        {
            if (BaseImage == null || (!updateShapes && !updateEdges))
                return;

            DepthStencilBuffer old = GraphicsDevice.DepthStencilBuffer;
            GraphicsDevice.DepthStencilBuffer = DSBuffer;
            
            GraphicsDevice.SetRenderTarget(0, RT);
            DrawBlurredImage(BaseImage, blurRadius);
            GraphicsDevice.SetRenderTarget(0, null);
            if(updateEdges)
                DrawEdgeImage(RT.GetTexture(), edgeThresh);

            if (updateShapes)
            {
                ReduceColorRange(RT, 4);

                Texture2D starryImage = CreateStarryNightImage();

                List<LineData> lines = new List<LineData>();
                //lines.AddRange(ZuluParlorTrick(starryImage));
                //lines.AddRange(DrawShadeLinesFromImage(starryImage));

                switch (shapeMode)
                {
                    case ShapeMode.ZuluParlorTrick:
                        lines.AddRange(ZuluParlorTrick(starryImage));
                        break;
                    default:
                        lines.AddRange(DrawShadeLinesFromImage(starryImage));
                        break;
                }
                detectedLines = lines.ToArray();
            }
            updateEdges = false;
            updateShapes = false;
            GraphicsDevice.DepthStencilBuffer = old;
        }

        /*
        List<LineData> Kruskal(Vector2[] chambers)
        {
            List<LineData> path = new List<LineData>();
            PriorityQueue<double, LineData> arcs = new PriorityQueue<double, LineData>();
            

            List<LineData> pathes = new List<LineData>();
            for (int i = 0; i < paths.Length; i++)
            {
                double diffX = path[i].start.X - path[i].end.X;
                double diffY = path[i].start.Y - path[i].end.Y;
                double dist = Math.Sqrt(diffX * diffX + diffY * diffY);
                arcs.Enqueue(paths[i], dist);
            }

            List<SortedList<int, Vector2>> chamberSets = new List<SortedList<int, Vector2>>();
            for (int i = 0; i < chambers.Length; i++)
            {
                SortedList<int, Vector2> chamber = new SortedList<int, Vector2>();
                chamber.Add(GetPathIndex(chambers[i]), chambers[i]);
                chamberSets.Add(chamber);
            }

            while (arcs.Count > 0)
            {
                LineData currPath = arcs.ExtractMin();
                int indexA = 0;
                int indexB = 0;
                for (int j = 0; j < chamberSets.Count; j++)
                {
                    if (chamberSets[j].ContainsKey(GetPathIndex(currPath.start)))
                    {
                        indexA = j;
                        break;
                    }
                }

                for (int j = 0; j < chamberSets.Count; j++)
                {
                    if (chamberSets[j].ContainsKey(GetPathIndex(currPath.end)))
                    {
                        indexB = j;
                        break;
                    }
                }

                if (indexA != indexB)
                {
                    path.Add(currPath);
                    for (int j = 0; j < chamberSets[indexB].Count; j++)
                    {
                        int currKey = chamberSets[indexB].Keys[j];
                        if (!chamberSets[indexA].ContainsKey(currKey))
                            chamberSets[indexA].Add(currKey, chamberSets[indexB][currKey]);
                    }
                    chamberSets.RemoveAt(indexB);
                }
            }

            return path;
        }
        */

        void DrawImage(Texture2D texture)
        {
            ppEffect.Parameters["baseMap"].SetValue(texture);
            ppEffect.Parameters["InvRes"].SetValue(Vector2.One / new Vector2(texture.Width, texture.Height));
            ppEffect.CurrentTechnique = ppEffect.Techniques["DrawImage"];
            ppEffect.Begin();
            ppEffect.CurrentTechnique.Passes[0].Begin();
            quad.Render(Vector2.One * -1, Vector2.One);
            ppEffect.CurrentTechnique.Passes[0].End();
            ppEffect.End();
        }

        void DrawLines(ref LineData[] lines)
        {
            GraphicsDevice.RenderState.PointSize = 128;
            ppEffect.CurrentTechnique = ppEffect.Techniques["DrawBlack"];
            ppEffect.Begin();
            ppEffect.CurrentTechnique.Passes[0].Begin();
            for (int i = 0; i < lines.Length; i++)
                line.Render(lines[i].start, lines[i].end, lines[i].color);
            ppEffect.CurrentTechnique.Passes[0].End();
            ppEffect.End();
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.RenderState.CullMode = CullMode.None;

            bool renderLines = (updateShapes || updateEdges || settingsChanged);
   
            PerformImageProcessing();
                        
            if (BaseImage != null)
                DrawImage(BaseImage);

            if (drawHough || DrawShapes || drawEdge)
                GraphicsDevice.Clear(Color.White);

            if ((drawEdge || drawHough) && RTPing != null)
            {
                GraphicsDevice.RenderState.AlphaBlendEnable = true;
                //GraphicsDevice.RenderState.BlendFunction = BlendFunction.Subtract;
                GraphicsDevice.RenderState.SourceBlend = Blend.SourceAlpha;
                GraphicsDevice.RenderState.DestinationBlend = Blend.InverseSourceAlpha;
                DrawImage(RTPing.GetTexture());
                GraphicsDevice.RenderState.AlphaBlendEnable = false;
            }
            if (renderLines)
            {
                if (detectedLines != null && (DrawShapes || drawHough))
                    DrawLines(ref detectedLines);

                if (Outlines != null && (DrawOutlines || drawHough))
                {
                    for (int i = 0; i < Outlines.Count; i++)
                    {
                        LineData[] lines = Outlines[i];
                        DrawLines(ref lines);
                    }
                }
            }

            base.Draw(gameTime);
        }
    }
}
