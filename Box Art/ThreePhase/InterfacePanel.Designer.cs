﻿namespace ThreePhase
{
    partial class InterfacePanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.imagefileButton = new System.Windows.Forms.Button();
            this.edgeCheckBox = new System.Windows.Forms.CheckBox();
            this.houghCheckBox = new System.Windows.Forms.CheckBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fIleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.savePointsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comboBoxShapeType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.loadColorPaletteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comboBoxZuluParlor = new System.Windows.Forms.ComboBox();
            this.checkBoxShapes = new System.Windows.Forms.CheckBox();
            this.checkBoxOutlines = new System.Windows.Forms.CheckBox();
            this.loadPointsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchRadius = new ThreePhase.CameraGui.GenericParameter();
            this.shapeCount = new ThreePhase.CameraGui.GenericParameter();
            this.edgeBlurRadius = new ThreePhase.CameraGui.GenericParameter();
            this.edgeThreshParam = new ThreePhase.CameraGui.GenericParameter();
            this.BlurRadiusParam = new ThreePhase.CameraGui.GenericParameter();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(41, 47);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(151, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Image Filename";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(21, 75);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(215, 22);
            this.textBox1.TabIndex = 3;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // imagefileButton
            // 
            this.imagefileButton.Location = new System.Drawing.Point(245, 70);
            this.imagefileButton.Margin = new System.Windows.Forms.Padding(4);
            this.imagefileButton.Name = "imagefileButton";
            this.imagefileButton.Size = new System.Drawing.Size(65, 33);
            this.imagefileButton.TabIndex = 4;
            this.imagefileButton.Text = "...";
            this.imagefileButton.UseVisualStyleBackColor = true;
            this.imagefileButton.Click += new System.EventHandler(this.imagefileButton_Click);
            // 
            // edgeCheckBox
            // 
            this.edgeCheckBox.AutoSize = true;
            this.edgeCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edgeCheckBox.Location = new System.Drawing.Point(21, 636);
            this.edgeCheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.edgeCheckBox.Name = "edgeCheckBox";
            this.edgeCheckBox.Size = new System.Drawing.Size(189, 29);
            this.edgeCheckBox.TabIndex = 7;
            this.edgeCheckBox.Text = "Draw Edge Image";
            this.edgeCheckBox.UseVisualStyleBackColor = true;
            this.edgeCheckBox.CheckedChanged += new System.EventHandler(this.EdgeButton_CheckedChanged);
            // 
            // houghCheckBox
            // 
            this.houghCheckBox.AutoSize = true;
            this.houghCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.houghCheckBox.Location = new System.Drawing.Point(21, 673);
            this.houghCheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.houghCheckBox.Name = "houghCheckBox";
            this.houghCheckBox.Size = new System.Drawing.Size(239, 29);
            this.houghCheckBox.TabIndex = 8;
            this.houghCheckBox.Text = "Draw WYSIWYG Mode";
            this.houghCheckBox.UseVisualStyleBackColor = true;
            this.houghCheckBox.CheckedChanged += new System.EventHandler(this.HoughButton_CheckedChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fIleToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(500, 28);
            this.menuStrip1.TabIndex = 11;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fIleToolStripMenuItem
            // 
            this.fIleToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadColorPaletteToolStripMenuItem,
            this.savePointsToolStripMenuItem,
            this.loadPointsToolStripMenuItem});
            this.fIleToolStripMenuItem.Name = "fIleToolStripMenuItem";
            this.fIleToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fIleToolStripMenuItem.Text = "File";
            // 
            // savePointsToolStripMenuItem
            // 
            this.savePointsToolStripMenuItem.Name = "savePointsToolStripMenuItem";
            this.savePointsToolStripMenuItem.Size = new System.Drawing.Size(155, 24);
            this.savePointsToolStripMenuItem.Text = "Save Points";
            this.savePointsToolStripMenuItem.Click += new System.EventHandler(this.savePointsToolStripMenuItem_Click);
            // 
            // comboBoxShapeType
            // 
            this.comboBoxShapeType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxShapeType.FormattingEnabled = true;
            this.comboBoxShapeType.Location = new System.Drawing.Point(27, 574);
            this.comboBoxShapeType.Name = "comboBoxShapeType";
            this.comboBoxShapeType.Size = new System.Drawing.Size(209, 24);
            this.comboBoxShapeType.TabIndex = 12;
            this.comboBoxShapeType.SelectionChangeCommitted += new System.EventHandler(this.comboBoxShapeType_SelectedIndexChanged);
            this.comboBoxShapeType.SelectedIndexChanged += new System.EventHandler(this.comboBoxShapeType_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(31, 546);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(137, 25);
            this.label2.TabIndex = 13;
            this.label2.Text = "Shape Type:";
            // 
            // loadColorPaletteToolStripMenuItem
            // 
            this.loadColorPaletteToolStripMenuItem.Name = "loadColorPaletteToolStripMenuItem";
            this.loadColorPaletteToolStripMenuItem.Size = new System.Drawing.Size(155, 24);
            this.loadColorPaletteToolStripMenuItem.Text = "Load SVG";
            this.loadColorPaletteToolStripMenuItem.Click += new System.EventHandler(this.loadColorPaletteToolStripMenuItem_Click);
            // 
            // comboBoxZuluParlor
            // 
            this.comboBoxZuluParlor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxZuluParlor.FormattingEnabled = true;
            this.comboBoxZuluParlor.Location = new System.Drawing.Point(258, 574);
            this.comboBoxZuluParlor.Name = "comboBoxZuluParlor";
            this.comboBoxZuluParlor.Size = new System.Drawing.Size(159, 24);
            this.comboBoxZuluParlor.TabIndex = 15;
            this.comboBoxZuluParlor.SelectionChangeCommitted += new System.EventHandler(this.comboBoxZuluParlor_SelectionChangeCommitted);
            // 
            // checkBoxShapes
            // 
            this.checkBoxShapes.AutoSize = true;
            this.checkBoxShapes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxShapes.Location = new System.Drawing.Point(265, 636);
            this.checkBoxShapes.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxShapes.Name = "checkBoxShapes";
            this.checkBoxShapes.Size = new System.Drawing.Size(152, 29);
            this.checkBoxShapes.TabIndex = 17;
            this.checkBoxShapes.Text = "Draw Shapes";
            this.checkBoxShapes.UseVisualStyleBackColor = true;
            this.checkBoxShapes.CheckedChanged += new System.EventHandler(this.checkBoxShapes_CheckedChanged);
            // 
            // checkBoxOutlines
            // 
            this.checkBoxOutlines.AutoSize = true;
            this.checkBoxOutlines.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxOutlines.Location = new System.Drawing.Point(265, 673);
            this.checkBoxOutlines.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxOutlines.Name = "checkBoxOutlines";
            this.checkBoxOutlines.Size = new System.Drawing.Size(156, 29);
            this.checkBoxOutlines.TabIndex = 18;
            this.checkBoxOutlines.Text = "Draw Outlines";
            this.checkBoxOutlines.UseVisualStyleBackColor = true;
            this.checkBoxOutlines.CheckedChanged += new System.EventHandler(this.checkBoxOutlines_CheckedChanged);
            // 
            // loadPointsToolStripMenuItem
            // 
            this.loadPointsToolStripMenuItem.Name = "loadPointsToolStripMenuItem";
            this.loadPointsToolStripMenuItem.Size = new System.Drawing.Size(155, 24);
            this.loadPointsToolStripMenuItem.Text = "Load Points";
            this.loadPointsToolStripMenuItem.Click += new System.EventHandler(this.loadPointsToolStripMenuItem_Click);
            // 
            // searchRadius
            // 
            this.searchRadius.Location = new System.Drawing.Point(21, 287);
            this.searchRadius.Margin = new System.Windows.Forms.Padding(5);
            this.searchRadius.Maximum = 10;
            this.searchRadius.Minimum = 2;
            this.searchRadius.Name = "searchRadius";
            this.searchRadius.ParameterName = "Shape Radius";
            this.searchRadius.Size = new System.Drawing.Size(319, 79);
            this.searchRadius.TabIndex = 16;
            this.searchRadius.Value = 5;
            this.searchRadius.ValueChanged += new System.EventHandler(this.searchRadius_ValueChanged);
            // 
            // shapeCount
            // 
            this.shapeCount.Location = new System.Drawing.Point(21, 462);
            this.shapeCount.Margin = new System.Windows.Forms.Padding(5);
            this.shapeCount.Maximum = 60;
            this.shapeCount.Minimum = 1;
            this.shapeCount.Name = "shapeCount";
            this.shapeCount.ParameterName = "Shape Count";
            this.shapeCount.Size = new System.Drawing.Size(319, 79);
            this.shapeCount.TabIndex = 10;
            this.shapeCount.Value = 10;
            this.shapeCount.ValueChanged += new System.EventHandler(this.lineCount_ValueChanged);
            // 
            // edgeBlurRadius
            // 
            this.edgeBlurRadius.Location = new System.Drawing.Point(21, 376);
            this.edgeBlurRadius.Margin = new System.Windows.Forms.Padding(5);
            this.edgeBlurRadius.Maximum = 30;
            this.edgeBlurRadius.Minimum = 2;
            this.edgeBlurRadius.Name = "edgeBlurRadius";
            this.edgeBlurRadius.ParameterName = "Lines Per Shape";
            this.edgeBlurRadius.Size = new System.Drawing.Size(319, 79);
            this.edgeBlurRadius.TabIndex = 9;
            this.edgeBlurRadius.Value = 12;
            this.edgeBlurRadius.ValueChanged += new System.EventHandler(this.lineThreshold_ValueChanged);
            // 
            // edgeThreshParam
            // 
            this.edgeThreshParam.Location = new System.Drawing.Point(21, 210);
            this.edgeThreshParam.Margin = new System.Windows.Forms.Padding(5);
            this.edgeThreshParam.Maximum = 40;
            this.edgeThreshParam.Minimum = 1;
            this.edgeThreshParam.Name = "edgeThreshParam";
            this.edgeThreshParam.ParameterName = "Edge Threshold";
            this.edgeThreshParam.Size = new System.Drawing.Size(319, 79);
            this.edgeThreshParam.TabIndex = 6;
            this.edgeThreshParam.Value = 20;
            this.edgeThreshParam.ValueChanged += new System.EventHandler(this.edgeThreshParam_ValueChanged);
            // 
            // BlurRadiusParam
            // 
            this.BlurRadiusParam.Location = new System.Drawing.Point(21, 124);
            this.BlurRadiusParam.Margin = new System.Windows.Forms.Padding(5);
            this.BlurRadiusParam.Maximum = 100;
            this.BlurRadiusParam.Minimum = 0;
            this.BlurRadiusParam.Name = "BlurRadiusParam";
            this.BlurRadiusParam.ParameterName = "Blur Radius";
            this.BlurRadiusParam.Size = new System.Drawing.Size(319, 79);
            this.BlurRadiusParam.TabIndex = 5;
            this.BlurRadiusParam.Value = 4;
            this.BlurRadiusParam.ValueChanged += new System.EventHandler(this.BlurRadiusParam_ValueChanged);
            // 
            // InterfacePanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 732);
            this.Controls.Add(this.checkBoxOutlines);
            this.Controls.Add(this.checkBoxShapes);
            this.Controls.Add(this.searchRadius);
            this.Controls.Add(this.comboBoxZuluParlor);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBoxShapeType);
            this.Controls.Add(this.shapeCount);
            this.Controls.Add(this.edgeBlurRadius);
            this.Controls.Add(this.houghCheckBox);
            this.Controls.Add(this.edgeCheckBox);
            this.Controls.Add(this.edgeThreshParam);
            this.Controls.Add(this.BlurRadiusParam);
            this.Controls.Add(this.imagefileButton);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "InterfacePanel";
            this.Text = "InterfacePanel";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button imagefileButton;
        private ThreePhase.CameraGui.GenericParameter BlurRadiusParam;
        private ThreePhase.CameraGui.GenericParameter edgeThreshParam;
        private System.Windows.Forms.CheckBox edgeCheckBox;
        private System.Windows.Forms.CheckBox houghCheckBox;
        private ThreePhase.CameraGui.GenericParameter edgeBlurRadius;
        private ThreePhase.CameraGui.GenericParameter shapeCount;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fIleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem savePointsToolStripMenuItem;
        private System.Windows.Forms.ComboBox comboBoxShapeType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem loadColorPaletteToolStripMenuItem;
        private System.Windows.Forms.ComboBox comboBoxZuluParlor;
        private ThreePhase.CameraGui.GenericParameter searchRadius;
        private System.Windows.Forms.CheckBox checkBoxShapes;
        private System.Windows.Forms.CheckBox checkBoxOutlines;
        private System.Windows.Forms.ToolStripMenuItem loadPointsToolStripMenuItem;
    }
}