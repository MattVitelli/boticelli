﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

using Microsoft.Xna.Framework;
using ThreePhase.Utils;
namespace ThreePhase
{
    public partial class InterfacePanel : Form
    {
        float outlineAspectRatio = 1;
        static float paperAspect = 1.277f;
        public InterfacePanel()
        {
            InitializeComponent();

            var shapes = EnumUtil.GetValues<ShapeMode>();
            foreach (ShapeMode shape in shapes)
                comboBoxShapeType.Items.Add(shape);
            //comboBoxShapeType.SelectedIndex = 0;
            var flares = EnumUtil.GetValues<LineStyle>();
            foreach(LineStyle flare in flares)
                comboBoxZuluParlor.Items.Add(flare);
            //comboBoxZuluParlor.SelectedIndex = 0;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (File.Exists(textBox1.Text))
                Game1.Instance.LoadTexture(textBox1.Text);
            else
                MessageBox.Show("Invalid file!");
        }

        private void imagefileButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            if (dlg.ShowDialog() == DialogResult.OK)
                textBox1.Text = dlg.FileName;
        }

        private void BlurRadiusParam_ValueChanged(object sender, EventArgs e)
        {
            Game1.Instance.BlurRadius = ((float)BlurRadiusParam.Value*10.0f/(float)BlurRadiusParam.Maximum);
        }

        private void edgeThreshParam_ValueChanged(object sender, EventArgs e)
        {
            Game1.Instance.EdgeThreshold = ((float)edgeThreshParam.Value/(float)edgeThreshParam.Maximum)*0.07f;
        }

        private void EdgeButton_CheckedChanged(object sender, EventArgs e)
        {
            Game1.Instance.DrawEdges = edgeCheckBox.Checked;
        }

        private void HoughButton_CheckedChanged(object sender, EventArgs e)
        {
            Game1.Instance.DrawWYSIWYG = houghCheckBox.Checked;
        }

        private void lineThreshold_ValueChanged(object sender, EventArgs e)
        {
            Game1.Instance.ShapeResolution = edgeBlurRadius.Value;
        }

        private void lineCount_ValueChanged(object sender, EventArgs e)
        {
            Game1.Instance.ShapeCount = shapeCount.Value;
        }

        Vector2 GetProperAspectRatio(float aspectRatio)
        {
            float invAspectRatio = 1.0f / aspectRatio;
            float invPaperAspect = 1.0f / paperAspect;
            Vector2 scaleVec = new Vector2(aspectRatio, 1);
            if (aspectRatio > paperAspect)
            {
                scaleVec = new Vector2(1, invAspectRatio) * paperAspect;
            }
            else if (invAspectRatio > invPaperAspect)
            {
                scaleVec = new Vector2(invAspectRatio, 1);
            }

            return scaleVec;
        }

        private void SavePointsToFile(string filename, ref LineData[] lines)
        {
            float aspectRatio = Game1.Instance.GetImageAspectRatio();
            Vector2 scaleVec = GetProperAspectRatio(aspectRatio);
            using (FileStream fs = new FileStream(filename, FileMode.Create))
            {
                using (StreamWriter wr = new StreamWriter(fs))
                {
                    for (int i = 0; i < lines.Length; i++)
                    {
                        Vector2 pos = TransformPointForSaving(lines[i].start, scaleVec);                        
                        wr.WriteLine(pos.X + " " + pos.Y);
                    }
                    Vector2 posEnd = TransformPointForSaving(lines[lines.Length - 1].end, scaleVec);
                    wr.WriteLine(posEnd.X + " " + posEnd.Y);
                }
            }
        }

        private Vector2 TransformPointForSaving(Vector2 posHomogenous, Vector2 aspect)
        {
            Vector2 transformed = posHomogenous * new Vector2(0.5f, -0.5f) + Vector2.One * 0.5f;
            transformed = transformed * aspect;
            transformed.X = transformed.X + (paperAspect - aspect.X) * 0.5f;
            transformed.Y = transformed.Y + (1.0f - aspect.Y) * 0.5f;
            return transformed;
        }

        private void SaveOutlines(string filename, List<LineData[]> outlines)
        {
            float aspectRatio = outlineAspectRatio;
            Vector2 scaleVec = GetProperAspectRatio(aspectRatio);
            using (FileStream fs = new FileStream(filename, FileMode.Create))
            {
                using (StreamWriter wr = new StreamWriter(fs))
                {
                    for (int i = 0; i < outlines.Count; i++)
                    {
                        
                        LineData[] lines = outlines[i];
                        if (lines[0].color.R > 128 && lines[0].color.G > 128 && lines[0].color.B > 128)
                        {
                            for (int j = 0; j < lines.Length; j++)
                            {
                                Vector2 pos = TransformPointForSaving(lines[j].start, scaleVec);
                                wr.WriteLine(pos.X + " " + pos.Y);
                            }
                            Vector2 posEnd = TransformPointForSaving(lines[lines.Length - 1].end, scaleVec); 

                            wr.WriteLine(posEnd.X + " " + posEnd.Y);
                            wr.WriteLine("U");
                        }
                    }
                }
            }
        }

        private void savePointsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveDlg = new SaveFileDialog();
            saveDlg.Title = "Enter filename to save as";
            if (saveDlg.ShowDialog() == DialogResult.OK)
            {
                LineData[] lines = Game1.Instance.GetDetectedLines();
                ColorMode[] modes = Game1.Instance.GetColorPalette();
                List<LineData[]> outlines = Game1.Instance.Outlines;
                

                SortedList<string, List<LineData>> linesMap = new SortedList<string, List<LineData>>();
                if (lines != null)
                {
                    for (int i = 0; i < lines.Length; i++)
                    {
                        LineData currLine = lines[i];
                        for (int j = 0; j < modes.Length; j++)
                        {
                            if (currLine.color == modes[j].color)
                            {
                                if (!linesMap.ContainsKey(modes[j].name))
                                    linesMap.Add(modes[j].name, new List<LineData>());
                                linesMap[modes[j].name].Add(currLine);
                                break;
                            }
                        }
                    }
                    for (int i = 0; i < linesMap.Keys.Count; i++)
                    {
                        string currKey = linesMap.Keys[i];
                        if (currKey != "White")
                        {
                            string filename = saveDlg.FileName + "_" + currKey + ".txt";
                            LineData[] currLines = linesMap[currKey].ToArray();
                            SavePointsToFile(filename, ref currLines);
                        }
                    }
                }
                if (outlines != null)
                {
                    string outlineFileName = saveDlg.FileName + "_Outlines.txt";
                    SaveOutlines(outlineFileName, outlines);
                }
            }

        }

        private void comboBoxShapeType_SelectedIndexChanged(object sender, EventArgs e)
        {
            Game1.Instance.Shape = (ShapeMode)comboBoxShapeType.SelectedItem;   
        }

        private void comboBoxZuluParlor_SelectionChangeCommitted(object sender, EventArgs e)
        {
            Game1.Instance.ArtisticFlare = (LineStyle)comboBoxShapeType.SelectedItem;
        }

        private void searchRadius_ValueChanged(object sender, EventArgs e)
        {
            Game1.Instance.SearchRadius = searchRadius.Value;
        }

        private void checkBoxShapes_CheckedChanged(object sender, EventArgs e)
        {
            Game1.Instance.DrawShapes = checkBoxShapes.Checked;
        }

        private void checkBoxOutlines_CheckedChanged(object sender, EventArgs e)
        {
            Game1.Instance.DrawOutlines = checkBoxOutlines.Checked;
        }

        private void loadColorPaletteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDlg = new OpenFileDialog();
            openDlg.Title = "Load SVG File";
            if(openDlg.ShowDialog() == DialogResult.OK)
                Game1.Instance.Outlines = SVGParser.ParseSVG(openDlg.FileName, out outlineAspectRatio);
        }

        private void loadPointsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDlg = new OpenFileDialog();
            openDlg.Title = "Load Outline File";
            if (openDlg.ShowDialog() == DialogResult.OK)
            {
                using(FileStream fs = new FileStream(openDlg.FileName, FileMode.Open))
                {
                    using(StreamReader reader = new StreamReader(fs))
                    {
                        Game1.Instance.Outlines = new List<LineData[]>();
                        float aspectRatio = 747.0f/377.0f;
                        Vector2 pointScale = new Vector2(2.0f / aspectRatio, 2.0f);
                        while(!reader.EndOfStream)
                        {
                            LineData line = new LineData();
                            line.color = Microsoft.Xna.Framework.Graphics.Color.White;
                            
                            List<LineData> lines = new List<LineData>();
                            string text = reader.ReadLine();
                            string[] data = text.Split(' ');
                            line.start = new Vector2(float.Parse(data[0]), float.Parse(data[1])) * pointScale - Vector2.One;
                            line.start *= new Vector2(1, -1);
                            while(text != "U" && !reader.EndOfStream)
                            {
                                text = reader.ReadLine();
                                if(text != "U")
                                {
                                    data = text.Split(' ');
                                    line.end = new Vector2(float.Parse(data[0]), float.Parse(data[1])) * pointScale - Vector2.One;
                                    line.end *= new Vector2(1, -1);
                                    lines.Add(line);
                                    line.start = line.end;
                                }
                            }
                            Game1.Instance.Outlines.Add(lines.ToArray());
                            
                        }
                    }
                }
            }
        }
    }
}
