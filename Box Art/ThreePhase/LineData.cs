﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ThreePhase
{
    public struct LineData
    {
        public Vector2 start;
        public Vector2 end;
        public Color color;
    }

    public partial class RenderLine : Microsoft.Xna.Framework.DrawableGameComponent
    {
        #region Private Members

        VertexDeclaration vertexDecl = null;
        VertexPositionColor[] verts = null;
        short[] ib = null;

        #endregion


        #region Constructor
        public RenderLine()
            : base(Game1.Instance)
        {
            Game1.Instance.Components.Add(this);
            // TODO: Construct any child components here
        }
        #endregion


        #region LoadGraphicsContent

        protected override void LoadContent()
        {
            IGraphicsDeviceService graphicsService =
                (IGraphicsDeviceService)base.Game.Services.GetService(
                                            typeof(IGraphicsDeviceService));

            vertexDecl = new VertexDeclaration(
                                graphicsService.GraphicsDevice,
                                VertexPositionColor.VertexElements);

            verts = new VertexPositionColor[]
                        {
                            new VertexPositionColor(
                                new Vector3(0,0,0),
                                Color.White),
                            new VertexPositionColor(
                                new Vector3(0,0,0),
                                Color.White),
                        };

            ib = new short[] { 0, 1 };

        }
        #endregion

        #region void Render(Vector2 start, Vector2 end)
        public void Render(Vector2 start, Vector2 end, Color color)
        {
            IGraphicsDeviceService graphicsService = (IGraphicsDeviceService)
                base.Game.Services.GetService(typeof(IGraphicsDeviceService));

            GraphicsDevice device = graphicsService.GraphicsDevice;
            device.VertexDeclaration = vertexDecl;

            verts[0].Position.X = start.X;
            verts[0].Position.Y = start.Y;
            verts[0].Color = color;

            verts[1].Position.X = end.X;
            verts[1].Position.Y = end.Y;
            verts[1].Color = color;

            device.DrawUserIndexedPrimitives<VertexPositionColor>
                (PrimitiveType.LineList, verts, 0, 2, ib, 0, 1);
        }
        #endregion
    }
}
