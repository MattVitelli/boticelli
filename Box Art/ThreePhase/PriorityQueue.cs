﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ThreePhase
{
    public class PriorityQueue<P, T>
    {
        SortedDictionary<P, Queue<T>> dictionary = new SortedDictionary<P, Queue<T>>();
        int logSize = 0;

        public int Count
        {
            get { return logSize; }
        }

        public void Enqueue(T element, P priority)
        {
            logSize++;
            if (!dictionary.ContainsKey(priority))
            {
                dictionary.Add(priority, new Queue<T>());
            }
            dictionary[priority].Enqueue(element);
        }

        public T ExtractMin()
        {
            logSize--;
            var smallest = dictionary.First();
            T element = smallest.Value.Dequeue();
            if (smallest.Value.Count == 0)
                dictionary.Remove(smallest.Key);
            return element;
        }

        public T ExtractMax()
        {
            logSize--;
            var largest = dictionary.Last();
            T element = largest.Value.Dequeue();
            if (largest.Value.Count == 0)
                dictionary.Remove(largest.Key);
            return element;
        }
    }
}
