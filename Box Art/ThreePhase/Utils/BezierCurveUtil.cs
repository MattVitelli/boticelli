﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace ThreePhase.Utils
{
    public static class BezierCurveUtil
    {
        public static SortedList<string, int> binomailMemoizer = new SortedList<string, int>();

        //Recursively construct binomial coefficients w/ memoizer for increased performance
        static int GenerateBinomialCoeffs(int n, int k, SortedList<string, int> memoizer)
        {
            if (k == 0) return 1;
            if (n == 0) return 0;
            string key = n + " " + k;
            if(memoizer.ContainsKey(key)) return memoizer[key];

            int coeff = GenerateBinomialCoeffs(n - 1, k - 1, memoizer) + GenerateBinomialCoeffs(n - 1, k, memoizer);
            if(!memoizer.ContainsKey(key)) memoizer.Add(key, coeff);

            return coeff;
        }

        static Vector2 EvaluateAtPoint(ref Vector2[] points, float value)
        {
            float invT = 1.0f-value;
            int n = points.Length - 1;
            Vector2 sum = Vector2.Zero;
            for(int i = 0; i < points.Length; i++)
                sum += GenerateBinomialCoeffs(points.Length-1, i, binomailMemoizer) * (float)Math.Pow(invT, n-i) * (float)Math.Pow(value, i) * points[i];
            
            return sum;
        }

        public static void Test(Vector2[] points, float[] tVals)
        {
            int n = points.Length - 1;
            for (int i = 0; i < points.Length; i++)
            {
                int binomVal = GenerateBinomialCoeffs(n, i, binomailMemoizer);
                Console.WriteLine("[{0}, {1}]->{2}", n, i, binomVal);
            }
            
            for (int i = 0; i < tVals.Length; i++)
            {
                Vector2 pt = EvaluateAtPoint(ref points, tVals[i]);
                Console.WriteLine("f({0})->{1}", tVals[i], pt);
            }
        }

        public static LineData[] CurveToLines(Vector2[] points)
        {
            const int NUM_SUBDIVISIONS = 12;
            LineData[] lines = new LineData[NUM_SUBDIVISIONS];
            float invT = 1.0f / (float)NUM_SUBDIVISIONS;
            for (int i = 0; i < NUM_SUBDIVISIONS; i++)
            {
                lines[i].start = (i > 0) ? lines[i - 1].end : points[0];
                lines[i].end = EvaluateAtPoint(ref points, (i + 1) * invT);
            }

            return lines;
        }
    }
}
