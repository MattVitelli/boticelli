﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace ThreePhase.Utils
{
    public static class ColorUtil
    {
        public static Vector3 RGBToHSL(Vector3 color)
        {
            Vector3 hsv = Vector3.Zero;
            float min, max, delta;
            min = Math.Min(Math.Min(color.X, color.Y), color.Z);
            max = Math.Max(Math.Max(color.X, color.Y), color.Z);
            hsv.Z = max;				// v
            delta = max - min;
            if (max != 0)
                hsv.Y = delta / max;		// s
            else
            {
                // r = g = b = 0		// s = 0, v is undefined
                hsv.Y = 0;
                hsv.X = -1;
                return hsv;
            }
            if (color.X == max)
                hsv.X = (color.Y - color.Z) / delta;		// between yellow & magenta
            else if (color.Y == max)
                hsv.X = 2 + (color.Z - color.X) / delta;	// between cyan & yellow
            else
                hsv.X = 4 + (color.X - color.Y) / delta;	// between magenta & cyan
            hsv.X *= 60;				// degrees
            if (hsv.X < 0)
                hsv.X += 360;
            hsv /= new Vector3(360, 255, 255);
            return hsv;
        }

        public static Vector3 RGBToXYZ(Vector3 rgb)
        {
            Vector3 colorPow = (rgb + 0.055f * Vector3.One) / 1.055f;
            colorPow.X = (float)Math.Pow(colorPow.X, 2.4f);
            colorPow.Y = (float)Math.Pow(colorPow.Y, 2.4f);
            colorPow.Z = (float)Math.Pow(colorPow.Z, 2.4f);
            Vector3 colorDiv = rgb / 12.92f;
            rgb.X = ( rgb.X > 0.04045 ) ? colorPow.X : colorDiv.X;
            rgb.Y = ( rgb.Y > 0.04045 ) ? colorPow.Y : colorDiv.Y;
            rgb.Z = ( rgb.Z > 0.04045 ) ? colorPow.Z : colorDiv.Z;

            Vector3 xyz;   
            //Observer. = 2°, Illuminant = D65
            xyz.X = Vector3.Dot(rgb, new Vector3(0.4124f, 0.3576f, 0.1805f));
            xyz.Y = Vector3.Dot(rgb, new Vector3(0.2126f, 0.7152f, 0.0722f));
            xyz.Z = Vector3.Dot(rgb, new Vector3(0.0193f, 0.1192f, 0.9505f));
            return xyz;
        }

        public static Vector3 xyz2cie(Vector3 color)
        {
            Vector3 refXYZ = new Vector3(0.95047f, 1f, 1.08883f);
            Vector3 var = color / refXYZ;
            Vector3 varPow = var;
            varPow.X = (float)Math.Pow(varPow.X, 1.0 / 3.0f);
            varPow.Y = (float)Math.Pow(varPow.Y, 1.0 / 3.0f);
            varPow.Z = (float)Math.Pow(varPow.Z, 1.0 / 3.0f);
            Vector3 varOff = (7.787f * var) + (16.0f / 116.0f)*Vector3.One;
            var.X = (var.X > 0.008856) ? varPow.X : varOff.X;
            var.Y = (var.Y > 0.008856) ? varPow.Y : varOff.Y;
            var.Z = (var.Z > 0.008856) ? varPow.Z : varOff.Z;

            Vector3 cie = new Vector3(1.16f * var.Y - 0.16f, 5.0f * (var.X - var.Y), 2.0f * (var.Y - var.Z));
            return cie;
        }
    }
}
