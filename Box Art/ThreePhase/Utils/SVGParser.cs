﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ThreePhase.Utils
{
    public static class SVGParser
    {
        public static void ParseSVGHeader(XmlDocument doc, ref Vector2 minBounds, ref Vector2 maxBounds)
        {
            XmlNodeList headerNode = doc.GetElementsByTagName("svg");
            foreach (XmlAttribute attribute in headerNode[0].Attributes)
            {
                if (attribute.Name == "viewBox")
                {
                    string[] data = attribute.Value.Split(' ');
                    minBounds.X = float.Parse(data[0]);
                    minBounds.Y = float.Parse(data[1]);
                    maxBounds.X = float.Parse(data[2]);
                    maxBounds.Y = float.Parse(data[3]);
                }
            }
        }

        static int ReadHex(char input)
        {
            if (input >= '0' && input <= '9')
                return input - '0';
            if (input >= 'a' && input <= 'f')
                return input + 10 - 'a';
            if (input >= 'A' && input <= 'F')
                return input + 10 - 'A';
            return 0;
        }

        static Color ParseColor(string text)
        {
            Color color = Color.Black;
            for (int i = 0; i < text.Length; i += 2)
            {
                byte value = (byte)((ReadHex(text[i]) << 4) | ReadHex(text[i + 1]));
                switch (i / 2)
                {
                    case 0:
                        color.R = value;
                        break;
                    case 1:
                        color.G = value;
                        break;
                    case 2:
                        color.B = value;
                        break;
                    default:
                        color.A = value;
                        break;
                }
            }
            return color;
        }

        static bool IsPathTolken(string tolken)
        {
            if (tolken == "C")
                return true;
            if (tolken == "M")
                return true;
            if (tolken == "L")
                return true;

            return false;
        }

        static int GetIndexOfNextTolken(string[] tolkens, int currTolken)
        {
            while (currTolken < tolkens.Length)
            {
                if (IsPathTolken(tolkens[currTolken]))
                    break;

                currTolken++;
            }
            return currTolken;
        }

        static LineData[] ParsePath(Color color, string text)
        {
            string[] tolkens = text.Split(' ');
            int currTolken = 0;

            List<LineData> lines = new List<LineData>();
            Vector2 origin = Vector2.Zero;
            while (currTolken < tolkens.Length)
            {
                switch (tolkens[currTolken])
                {
                    case "M":
                        origin.X = float.Parse(tolkens[currTolken + 1]);
                        origin.Y = float.Parse(tolkens[currTolken + 2]);
                        currTolken += 2;
                        break;
                    case "L":
                        Vector2 end;
                        end.X = float.Parse(tolkens[currTolken + 1]);
                        end.Y = float.Parse(tolkens[currTolken + 2]);
                        LineData line = new LineData();
                        line.start = origin;
                        line.end = end;
                        lines.Add(line);
                        currTolken += 2;
                        origin = end;
                        break;
                    case "C":
                        int pointCount = (GetIndexOfNextTolken(tolkens, currTolken + 1) - currTolken) / 2;
                        Vector2[] points = new Vector2[pointCount];
                        points[0] = origin;
                        for(int i = 1; i < points.Length; i++)
                        {
                            int index = (i - 1) * 2 + currTolken+1;
                            points[i] = new Vector2(float.Parse(tolkens[index]), float.Parse(tolkens[index + 1]));
                        }
                        lines.AddRange(BezierCurveUtil.CurveToLines(points));
                        origin = points[points.Length - 1];
                        currTolken += pointCount * 2;
                        break;
                }
                currTolken++;
            }

            LineData[] finalLines = lines.ToArray();
            for (int i = 0; i < finalLines.Length; i++)
                finalLines[i].color = color;

            return finalLines;
        }

        static void ParsePaths(XmlDocument doc, List<LineData[]> linesList)
        {
            XmlNodeList nodes = doc.GetElementsByTagName("path");
            foreach (XmlNode node in nodes)
            {
                string dataString = string.Empty;
                Color color = Color.TransparentBlack;
                foreach (XmlAttribute attribute in node.Attributes)
                {
                    if (attribute.Name == "fill")
                        color = ParseColor(attribute.Value.Substring(1));
                    if (attribute.Name == "d")
                        dataString = attribute.Value;
                }
                linesList.Add(ParsePath(color, dataString));
            }
        }

        static void ResizeLineData(Vector2 docMin, Vector2 docMax, List<LineData[]> parsedData)
        {
            Vector2 scale = docMax - docMin;
            for (int i = 0; i < parsedData.Count; i++)
            {
                for (int j = 0; j < parsedData[i].Length; j++)
                {
                    LineData currLine = parsedData[i][j];
                    Vector2 lineStart = (currLine.start - docMin) / scale;
                    Vector2 lineEnd = (currLine.end - docMin) / scale;
                    currLine.start = (lineStart * 2.0f - Vector2.One) * new Vector2(1, -1);
                    currLine.end = (lineEnd * 2.0f - Vector2.One) * new Vector2(1, -1);
                    parsedData[i][j] = currLine;
                }
            }
        }

        public static List<LineData[]> ParseSVG(string filename, out float aspectRatio)
        {
            string xmlText = string.Empty;
            using (FileStream fs = new FileStream(filename, FileMode.Open))
            {
                using (StreamReader reader = new StreamReader(fs))
                {
                    xmlText = reader.ReadToEnd();
                }
            }

            xmlText = xmlText.Replace("<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">", string.Empty);

            xmlText = xmlText.Replace("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>", "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\" ?>");

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlText);
            Vector2 docMin = Vector2.Zero;
            Vector2 docMax = Vector2.Zero;
            ParseSVGHeader(xmlDoc, ref docMin, ref docMax);
            Vector2 docScale = docMax-docMin;
            aspectRatio = docScale.X / docScale.Y;

            List<LineData[]> parsedData = new List<LineData[]>();
            ParsePaths(xmlDoc, parsedData);
            ResizeLineData(docMin, docMax, parsedData);
            
            return parsedData;
        }
    }
}
