//
//  CanvasPointTokenizer.cpp
//  
//
//  Created by Anthony Mainero on 5/28/12.
//  Copyright 2012 Stanford University. All rights reserved.
//

#include "CanvasPointTokenizer.h"
#include <fstream>

using namespace std;

CanvasPointTokenizer::CanvasPointTokenizer(ifstream &pointFile)

{

   fileProgress = 0.0f;
	while (!pointFile.eof())
	{
		string nline = "";
		pointFile >> nline;
		//nline = removeFormatting(nline); // This shouldn't be an issue.
		commands.push(nline);
	}
	num_chars = commands.size();
}

CanvasPointTokenizer::~CanvasPointTokenizer()
{

}

bool CanvasPointTokenizer::hasMoreTokens()
{
    return !commands.empty();
}

string CanvasPointTokenizer::nextToken()
{
    string toReturn = commands.front();
    commands.pop();
	int dataLeft = commands.size();
	fileProgress = 100.0*(1.0 - (float)dataLeft/(float)num_chars);
    return toReturn;
}

string CanvasPointTokenizer::removeFormatting(string line)
{
    string newLine = "";
    for (int i = 0; i < line.length(); i++) {
        if (line[i] != '\n' && line[i] != ' ')
            newLine += line[i];
    }
    return newLine;
}