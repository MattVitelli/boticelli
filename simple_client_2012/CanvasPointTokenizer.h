//
//  CanvasPointTokenizer.h
//  
//
//  Created by Anthony Mainero on 5/28/12.
//  Copyright 2012 Stanford University. All rights reserved.
//

#ifndef _CanvasPointTokenizer_h
#define _CanvasPointTokenizer_h

#include <iostream>
#include <queue>
#include <string>

using namespace std;

class CanvasPointTokenizer
{
private:
    
    queue<string> commands;
    string removeFormatting(string line);
	int num_chars;
    
public:
    
    CanvasPointTokenizer(ifstream &pointFile);
    ~CanvasPointTokenizer(); //destructor
    bool hasMoreTokens();
    string nextToken();
	float fileProgress;
    
};

#endif
