
#include "Robot.h"
#include <fstream>
#include <iostream>

float liftDist = 0.015f;
float liftForward = 0.03f;
float liftBack = 0.015f;
bool moveSlowFlag = false;
Robot::Robot()
{
	robot = new RobotCom(); // Connecting to the QNX
	cout << "Release the robot's brakes, then press enter.\n";
	cin.get();
	cout << "Great! Now reposition the robot so it can safely move to home position.\n";
	cout << "Press enter when the robot and your colleagues are safely out of the work space.\n";
	float q[6];
	memset(q, 0, sizeof(float)*6);
	robot->control(FLOATMODE, q, 6);
	cin.get();
	LoadCalibrationBoard();
	Reset();
	planeOff = 0.000f;
	planeLift = 0.03f;
	liftForward = planeLift;
	endEffectorType = 0;
	liftFromPlane = false;
	isShading = true;
}

Robot::~Robot()
{
	delete robot;
}

void Robot::LiftOffBoard()
{
	liftFromPlane = true;
	PrVector3 deltaAUnit(deltaA);
	PrVector3 planePoint = GetCurrentPoint();
	currPointToVisit = planePoint + planeLift*planeNormal - liftForward*deltaAUnit.normalize();
	//Move to the new point;
	BeginMotion();
	if(pointsToVisit.empty()) return;
	currPointToVisit = pointsToVisit.front() + planeLift*planeNormal - liftBack*deltaAUnit;
	//Redundant code for reduced velocity limit:
	BeginMotion();
	currPointToVisit = pointsToVisit.front();
	moveSlowFlag = true;
	BeginMotion();
	pointsToVisit.pop();
	liftFromPlane = false;
}


void Robot::ChangeTool(string newColor, int newImplement)
{
	float q[6] = {-45, -100, 220, 0, -90, 0};
	robot->jointControl(JTRACK, q[0], q[1], q[2], q[3], q[4], q[5]); 
	SetFloatMode();

	cout<<"Please give me "<<newColor<<" "<<((newImplement == crayonType)?("CRAYON"):("MARKER"))<<endl;


	if(newImplement !=endEffectorType)
	{
		GetPointFromUser("upper-left", origin);
		float posOpSpace[7]; //operational space position
		robot->getStatus(GET_IPOS, posOpSpace);
		originOrientation.setValues(posOpSpace+3);
		SaveCalibrationBoard();
		endEffectorType = newImplement;
	}

}

void Robot::Reset()
{
	float q[6] = {0, -60, 180, 0, -90, 0};
	robot->jointControl(JTRACK, q[0], q[1], q[2], q[3], q[4], q[5]); 
	float posJSpace[6]; //operational space position
	do{
		robot->getStatus(GET_JPOS, posJSpace);
	}while(abs(posJSpace[0] - q[0]) > 3.0f);
}

void Robot::CalibrateBoard()
{
	int status = GetPointFromUser("upper-left", origin);
	if (!status)
	{
		float posOpSpace[7]; //operational space position
		robot->getStatus(GET_IPOS, posOpSpace);
		originOrientation.setSize(4);
		originOrientation.setValues(posOpSpace+3);

		GetPointFromUser("lower-left", deltaA);
		GetPointFromUser("upper-right", deltaB);
		deltaA -= origin;
		deltaB -= origin;
		planeNormal = deltaA.cross(deltaB);
		planeNormal.normalize();
		planeDist = -planeNormal.dot(origin);
		SaveCalibrationBoard();
	}
}

void Robot::QuickCalibrate()
{
	int status = GetPointFromUser("upper-left", origin);
	if (!status)
	{
		float posOpSpace[7]; //operational space position
		robot->getStatus(GET_IPOS, posOpSpace);
		originOrientation.setSize(4);
		originOrientation.setValues(posOpSpace+3);
		SaveCalibrationBoard();
	}
}

void Robot::LoadCalibrationBoard()
{
	std::ifstream calibFile("calib.txt");
	if(!calibFile)
		return;
	originOrientation.setSize(4, true);
	calibFile >> origin[0]; calibFile >> origin[1]; calibFile >> origin[2];
	calibFile >> deltaA[0]; calibFile >> deltaA[1]; calibFile >> deltaA[2];
	calibFile >> deltaB[0]; calibFile >> deltaB[1]; calibFile >> deltaB[2];
	calibFile >> originOrientation[0]; calibFile >> originOrientation[1]; calibFile >> originOrientation[2] >> originOrientation[3];
	calibFile >> endEffectorType;
	planeNormal = deltaA.cross(deltaB);
	planeNormal.normalize();
	calibFile.close();
}

void Robot::SaveCalibrationBoard()
{
	std::ofstream calibFile("calib.txt");
	calibFile << origin[0] << " " << origin[1] << " " << origin[2] << endl; 
	calibFile << deltaA[0] << " " << deltaA[1] << " " << deltaA[2] << endl; 
	calibFile << deltaB[0] << " " << deltaB[1] << " " << deltaB[2] << endl;
	calibFile << originOrientation[0] << " " << originOrientation[1] << " " << originOrientation[2] << " "<< originOrientation[3] << endl;
	calibFile << endEffectorType;
	calibFile.close();
}

int Robot::GetPointFromUser(std::string message, PrVector3 &vec)
{
	float q[6];
	vec  = origin;
	memset(q, 0, sizeof(float)*6);
	robot->control(FLOATMODE, q, 6);
	char curr = '0';
	while(curr != 'e')
	{
		cout << "Please guide the robot to the " << message << " corner of the canvas\n";
		cout << "Press 'e' and enter when you are ready to continue. Press 'a' to abort.\n";
		curr = cin.get();
		cout << "You entered " << curr << endl;
		if(curr == 'a') return 1;
	}

	vec = GetCurrentPoint();
	cout << "Successfully captured point!\n";
	return 0;
}
static float sumEps = 0.0f;
void Robot::AddPointFromImage(float x, float y)
{
	float eps = 0.007f;
	
	PrVector3 pointInPlane = origin + deltaA*y + deltaB*x - planeOff*planeNormal;
	PrVector3 temp = planeNormal;
	if(!pointsToVisit.empty())
	{
		float distPoints = (pointsToVisit.back() - pointInPlane).length();
		sumEps += distPoints;
		if(sumEps < eps) 
		{
			return;
		}
	}
	sumEps = 0.0f;
	pointsToVisit.push(pointInPlane);
}

void Robot::OnUpdate()
{
	float progress = 100.0f*(1.0f - (float)pointsToVisit.size()/(float)pointsSizeInitial);
	printf("Local Progress: %2f \xd", progress);
	isDrawing = (!pointsToVisit.empty());
	if(isDrawing){
		BeginMotion();
	}

}

void Robot::BeginMotion()
{
	if (pointsToVisit.empty()) return;
	PrVector3 diffPoint = GetCurrentPoint() - pointsToVisit.front();
	if(isShading && !liftFromPlane && diffPoint.length()>liftDist && diffPoint.length()<(2.0*deltaA.length()))
	{
		LiftOffBoard();
		return;
	}
	else if(!liftFromPlane)
	{
		currPointToVisit = pointsToVisit.front();
		pointsToVisit.pop();
	}
	float opPos[7];
	currPointToVisit.getValues(opPos);
	originOrientation.getValues(opPos+3);
	if(moveSlowFlag)
	{
		robot->control( NTRACK, opPos, 7); //Reduced velocity limit
		moveSlowFlag = false;
	}
	else
		robot->control( TRACK, opPos, 7);
	const float eps = 0.007f;
	const float velocityEps = 0.01f;
	int MAX_COUNT = 100;
	int counter = 0;
	while(diffPoint.length() > eps)
	{
		diffPoint = GetCurrentPoint() - currPointToVisit;
		counter++;
		PrVector currJVel = GetJointVelocity();
		if(counter>MAX_COUNT && currJVel.length()<velocityEps)
		{
			counter = 0;
			robot->control( NTRACK, opPos, 7); //Failsafe condition
		}
	}
}

PrVector3 Robot::GetCurrentPoint()
{
	float posOpSpace[7]; //operational space position
	robot->getStatus(GET_IPOS, posOpSpace);
	PrVector3 vec;
	vec.setValues(posOpSpace);
	return vec;
}

PrVector Robot::GetJointVelocity()
{
	PrVector jointVel(6);
	robot->getStatus(GET_JVEL, jointVel.dataPtr());
	return jointVel;
}

void Robot::SetDrawing(bool state)
{
	isDrawing = state;
	if(isDrawing)
	{
		pointsSizeInitial = pointsToVisit.size();
		if(pointsSizeInitial>0)
		BeginMotion();
	}
}

void Robot::SetFloatMode()
{
	float q[6];
	memset(q, 0, sizeof(float)*6);
	robot->control(FLOATMODE, q, 6);
}

void Robot::DrawBounds()
{
	/*
	for(int i = 0; i <= 1; i++)
	{
		for(int j = 0; j <= 1; j++)
		{
			AddPointFromImage(i, j);
		}
	}
	*/
	AddPointFromImage(0,0);
	AddPointFromImage(0,1);
	AddPointFromImage(1,0);
	AddPointFromImage(1,1);
	
}