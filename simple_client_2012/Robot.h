#ifndef _ROBOT_H
#define _ROBOT_H

#include "robotCom.h"
#include "prvector.h"
#include "prvector3.h"
#include <iostream>
#include <queue>
#include <string>

using namespace std;

class Robot
{
public:
	Robot();

	~Robot();

	void Reset();

	void OnUpdate();

	void CalibrateBoard();
	
	void QuickCalibrate();

	void DrawBounds();

	void BeginMotion();

	void SetDrawing(bool state);

	void AddPointFromImage(float x, float y);

	bool IsDrawing() { return isDrawing; }
	
	void SetFloatMode();

	void LiftOffBoard();

	void ChangeTool(string newColor, int newImplement);
	

private:
	RobotCom* robot;
	PrVector3 planeNormal;
	float	  planeDist;
	PrVector3 origin;
	PrVector3 deltaA;
	PrVector3 deltaB;
	PrVector originOrientation;
	float planeOff;
	float planeLift;
	bool liftFromPlane;
	bool isShading;
	int endEffectorType;
	int pointsSizeInitial;

	bool isDrawing;

	queue<PrVector3> pointsToVisit;
	PrVector3 currPointToVisit;

	int GetPointFromUser(std::string message, PrVector3 &vec);

	void SaveCalibrationBoard();

	void LoadCalibrationBoard();

	PrVector3 GetCurrentPoint();

	PrVector GetJointVelocity();
};


static int crayonType = 0;
static int markerType = 1;
static int charcoalType = 2;

#endif