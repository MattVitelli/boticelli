#include <Winsock.h>
#include <windows.h>
#include "robotCom.h"
#include "../CanvasPointTokenizer.h"
#include "../Robot.h"
//you will need to change PrNetworkDefn and Robot.cpp based on the 
//QNX computer you are using
//Copy cs225a.h from your cs225asim directory
#include <iostream>
#include <tchar.h>
#include "prvector.h"
#include "prvector3.h"
#include "prmatrix.h"
#include "prmatrix3.h"
#include <fstream>
#include "param.h"
#include <string.h>
#include <stdlib.h>
#include "math.h"

using namespace std;
struct CanvasPoint 
{
    float x,y;
};

void PrintInvalidInput(int input, int minValue, int maxValue);

void DrawImageFromFile();

vector<CanvasPoint> *GetCanvasPoints(CanvasPointTokenizer* tokenizer, int &mode);

Robot *myRobot;

void DisplayMainMenu()
{
	while(true)
	{
		cout << "Please select an option:\n";
		cout << "1)\tReset Robot to Default Position\n";
		cout << "2)\tSet Robot in Float Mode\n";
		cout << "3)\tCalibrate Drawing Board\n";
		cout << "4)\tCalibrate Origin only\n";
		cout << "5)\tDraw a Sketch\n";
		cout << "6)\tExit\n";
		char input;
		cin >> input;
		Sleep(100);
		switch(input)
		{
		case '1':
			myRobot->Reset();
			break;
		case '2':
			myRobot->SetFloatMode();
			break;
		case '3':
			myRobot->CalibrateBoard();
			break;
		case '4':
			myRobot->QuickCalibrate();
			break;
		case '5':
			DrawImageFromFile();
			break;
		case '6':
			return;
		default:
			PrintInvalidInput(input, 1, 5);
			break;
		}
	}
}

void PrintInvalidInput(int input, int minValue, int maxValue)
{
	cout << "Invalid input. You entered " << input <<".\n Please enter a number between " 
		<< minValue << " and " << maxValue << endl;
}

void GetCoordinates(string xString, string yString, float &x, float &y)
{
	x = atof(xString.c_str());
	y = atof(yString.c_str());
}

void GetColornImplement(ifstream &colorFile, string &color, int &implement)
{
	colorFile>>color;
	string implementString = " ";
	colorFile>>implementString;
	implement = atof(implementString.c_str());
}


void DrawImageFromFile()
{
	cout << "Please enter a filename containing image points.\n";
	string filename = "";
	cin >> filename;
	ifstream pointFile(filename.c_str());
	if (!pointFile)
	{
		cout << "Bad filename." << endl;
		return;
	}
	CanvasPointTokenizer* tokenizer = new CanvasPointTokenizer(pointFile);
	int mode = 0; //mode = 0: starting, 3: done drawing, 1: lift after drawing, 2: change tool after drawing 
	int emode = 0;
	while(mode!=3)
	{
		emode = mode;
		vector<CanvasPoint> *points = NULL;
		points = GetCanvasPoints(tokenizer,mode);
		for(int i = 0; i < points->size(); i++)
		{
			CanvasPoint point = points->at(i);
			myRobot->AddPointFromImage(point.x, point.y);
		}
		if(emode == 2)
		{
			int implement = 1;
			string color = " ";
			GetColornImplement(pointFile, color, implement);
			myRobot->ChangeTool(color, implement);
		}
		if(emode == 1)
		{
			myRobot->LiftOffBoard();
		}
		myRobot->SetDrawing(true);
		printf("\n");
		while(myRobot->IsDrawing())
		{
			bool temp = myRobot->IsDrawing();
			myRobot->OnUpdate();
		}
		delete points;
	}
	
	myRobot->SetFloatMode();
	delete tokenizer;
}


vector<CanvasPoint> *GetCanvasPoints(CanvasPointTokenizer* tokenizer, int &mode) 
{
    vector<CanvasPoint> *cpv = new vector<CanvasPoint>();
	mode = 0;
    while (tokenizer->hasMoreTokens())
    {
		CanvasPoint cp;
		string nline = tokenizer->nextToken();
		printf("Boticelli's Progress: %2f \xd", tokenizer->fileProgress);
		if(nline[0] == 'C'){
			mode = 2; 
			return cpv;
		}
		else if(nline[0] == 'U'){
			mode = 1;		
			return cpv;	
		}
		else
		{
			float x;
			float y;
			if(tokenizer->hasMoreTokens())
			{
				string yline = tokenizer->nextToken();
				string xline = nline;
				GetCoordinates(xline, yline, x, y);
				cp.x = x;
				cp.y = y;
				cpv->push_back(cp);
			}
		}
    }
	mode = 3;
    return cpv;
}


int _tmain(int argc, _TCHAR* argv[])
{
	myRobot = new Robot();
	myRobot->SetFloatMode();
	DisplayMainMenu();
	delete myRobot;

	return 0;
}